package com.ndira.sponsor.portal.testsV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import com.ndira.sponsor.portal.librariesV2.AccountOpeningPaymentClient;
import com.ndira.sponsor.portal.librariesV2.AddFeeScheduleinFees;
import com.ndira.sponsor.portal.librariesV2.AddInvestmentNegative;
import com.ndira.sponsor.portal.librariesV2.AddingDepositories;
import com.ndira.sponsor.portal.librariesV2.AllocationsValidation;
import com.ndira.sponsor.portal.librariesV2.ClientRelationshipforSponsorRelationshipAdvisor;
import com.ndira.sponsor.portal.librariesV2.ClientRelationshipforSponsorRelationshipprovider;
import com.ndira.sponsor.portal.librariesV2.ClientRelationshipforSponsorrelationshipReferrer;
import com.ndira.sponsor.portal.librariesV2.ClientSignupStep3headingforAdvisor;
import com.ndira.sponsor.portal.librariesV2.ClientSignupStep3headingforReferrer;
import com.ndira.sponsor.portal.librariesV2.CompanyEdit;
import com.ndira.sponsor.portal.librariesV2.CompanyOverview;
import com.ndira.sponsor.portal.librariesV2.DeleteSponsor;
import com.ndira.sponsor.portal.librariesV2.DepositoryInformationInStep3Validation;
import com.ndira.sponsor.portal.librariesV2.FeeScheduleNAmeDisplayinginOpenAccount;
import com.ndira.sponsor.portal.librariesV2.FeesValidation;
import com.ndira.sponsor.portal.librariesV2.ForgetPassword;
import com.ndira.sponsor.portal.librariesV2.ForgetPasswordNegative;
import com.ndira.sponsor.portal.librariesV2.InvalidLogin;
import com.ndira.sponsor.portal.librariesV2.InvalidSponsorSignup;
import com.ndira.sponsor.portal.librariesV2.InvestComponentsDepositoryStorageOptions;
import com.ndira.sponsor.portal.librariesV2.InvestComponentsEnableTransactionFee;
import com.ndira.sponsor.portal.librariesV2.InvestmentComponentsAllAvailableFunds;
import com.ndira.sponsor.portal.librariesV2.InvestmentComponentsClientRelationship;
import com.ndira.sponsor.portal.librariesV2.InvestmentComponentsCourtesycall;
import com.ndira.sponsor.portal.librariesV2.InvestmentComponentsInvestmentTermsandConditions;
import com.ndira.sponsor.portal.librariesV2.InvestmentComponentsSave;
import com.ndira.sponsor.portal.librariesV2.InvestmentComponentsSetAllocation;
import com.ndira.sponsor.portal.librariesV2.InvestmentComponentsdefaultforAdmin;
import com.ndira.sponsor.portal.librariesV2.InvestmentComponentsdefaultforReferrer;
import com.ndira.sponsor.portal.librariesV2.InvestmentComponentsdefaultforprovider;
import com.ndira.sponsor.portal.librariesV2.Logout;
import com.ndira.sponsor.portal.librariesV2.ManageRemoveTeamMemberNegative;
import com.ndira.sponsor.portal.librariesV2.ManageTeamNegative;
import com.ndira.sponsor.portal.librariesV2.MarketingMaterials;
import com.ndira.sponsor.portal.librariesV2.NDIRAHowDidYouKnow;
import com.ndira.sponsor.portal.librariesV2.NDIRALoginAdobeSignWidget;
import com.ndira.sponsor.portal.librariesV2.NDIRALoginDidYouKnow;
import com.ndira.sponsor.portal.librariesV2.NDIRALoginDidYouKnowDelete;
import com.ndira.sponsor.portal.librariesV2.NDIRALoginMarketingMaterials;
import com.ndira.sponsor.portal.librariesV2.NDIRALoginRemoveTeammemberNegative;
import com.ndira.sponsor.portal.librariesV2.NDIRALoginSponsorUserNegative;
import com.ndira.sponsor.portal.librariesV2.NDIRALoginWebinars;
import com.ndira.sponsor.portal.librariesV2.NDIRAMarketingMaterialsNegative;
import com.ndira.sponsor.portal.librariesV2.NDIRAMessageNegative;
import com.ndira.sponsor.portal.librariesV2.NDIRAUsersignupNegativeStep1;
import com.ndira.sponsor.portal.librariesV2.NDIRAUsersignupNegativeStep2;
import com.ndira.sponsor.portal.librariesV2.NDIRAUsersignupNegativeStep3;
import com.ndira.sponsor.portal.librariesV2.NDIRAWire_Instructions;
import com.ndira.sponsor.portal.librariesV2.NDIRAWire_Instructions_Negative;
import com.ndira.sponsor.portal.librariesV2.NDTCORemove;
import com.ndira.sponsor.portal.librariesV2.NDTCOResetPassword;
import com.ndira.sponsor.portal.librariesV2.NDTCOUsers;
import com.ndira.sponsor.portal.librariesV2.NDTCOUsersNegative;
import com.ndira.sponsor.portal.librariesV2.NDiraAddSponsorUsers;
import com.ndira.sponsor.portal.librariesV2.NDiraDepositories;
import com.ndira.sponsor.portal.librariesV2.NDiraLogin;
import com.ndira.sponsor.portal.librariesV2.NDiraloginDepositories;
import com.ndira.sponsor.portal.librariesV2.NDiraloginFeeSchedules;
import com.ndira.sponsor.portal.librariesV2.NdiraLoginUsers;
import com.ndira.sponsor.portal.librariesV2.Prepayment;
import com.ndira.sponsor.portal.librariesV2.Sitedata_Add_Fee_details;
import com.ndira.sponsor.portal.librariesV2.Sitedata_FeeSchedule_Update;
import com.ndira.sponsor.portal.librariesV2.Sponsersignup;
import com.ndira.sponsor.portal.librariesV2.SponsorAddInvestiment;
import com.ndira.sponsor.portal.librariesV2.SponsorAddTeamMember;
import com.ndira.sponsor.portal.librariesV2.SponsorChangePassword;
import com.ndira.sponsor.portal.librariesV2.SponsorChangePasswordNegative;
import com.ndira.sponsor.portal.librariesV2.SponsorCompanyProfileUpdate;
import com.ndira.sponsor.portal.librariesV2.SponsorCurrentInvestments;
import com.ndira.sponsor.portal.librariesV2.SponsorDepositories;
import com.ndira.sponsor.portal.librariesV2.SponsorFees;
import com.ndira.sponsor.portal.librariesV2.SponsorInvestimentStatus;
import com.ndira.sponsor.portal.librariesV2.SponsorInvestments;
import com.ndira.sponsor.portal.librariesV2.SponsorManageTeamRemoveTeamMember;
import com.ndira.sponsor.portal.librariesV2.SponsorManageTeamResetPassword;
import com.ndira.sponsor.portal.librariesV2.SponsorMessageNegative;
import com.ndira.sponsor.portal.librariesV2.SponsorMessageToNDira;
import com.ndira.sponsor.portal.librariesV2.SponsorMyInfoUpdate;
import com.ndira.sponsor.portal.librariesV2.SponsorRegularLoginLogout;
import com.ndira.sponsor.portal.librariesV2.SponsorSignupFAQs;
import com.ndira.sponsor.portal.librariesV2.SponsorSignupMarketingMaterials;
import com.ndira.sponsor.portal.librariesV2.SponsorSignupWebinarVideos;
import com.ndira.sponsor.portal.librariesV2.SponsorUserRemove;
import com.ndira.sponsor.portal.librariesV2.SponsorUserResetpassword;
import com.ndira.sponsor.portal.librariesV2.SponsorUsersignupNegativeStep1;
import com.ndira.sponsor.portal.librariesV2.SponsorUsersignupNegativeStep2;
import com.ndira.sponsor.portal.librariesV2.SponsorUsersignupNegativeStep3;
import com.ndira.sponsor.portal.librariesV2.SponsorWireInstructions;
import com.ndira.sponsor.portal.librariesV2.Sponsorloginlogout;
import com.ndira.sponsor.portal.librariesV2.Sponsorlogout;
import com.ndira.sponsor.portal.librariesV2.Sponsormessages;
import com.ndira.sponsor.portal.librariesV2.UserSignUpwithSponser;
import com.ndira.sponsor.portal.librariesV2.UserSignupwithNDiralogin;
import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class NdiraTestV2 extends NDiraConstantsV2
{
	XLUtilsV2 xl = new XLUtilsV2();
	String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
	String tcsheet = "TestCases";
	String tssheet = "TestSteps";
	int tccount, tscount;
	String tcexe;
	String tcid, tsid, keyword;
	String tcres = "";
	boolean res = false;

	NDiraLogin nd = new NDiraLogin();
	UserSignUpwithSponser usp = new UserSignUpwithSponser();
	Sponsersignup sup = new Sponsersignup();
	CompanyEdit ce = new CompanyEdit();
	UserSignupwithNDiralogin usnl = new UserSignupwithNDiralogin();
	CompanyOverview co = new CompanyOverview();
	Logout out = new Logout();
	Sponsormessages sm = new Sponsormessages();
	SponsorInvestments si = new SponsorInvestments();
	SponsorFees sf = new SponsorFees();
	NDiraAddSponsorUsers su = new NDiraAddSponsorUsers();
	SponsorUserRemove sremove = new SponsorUserRemove();
	SponsorUserResetpassword srp = new SponsorUserResetpassword();
	MarketingMaterials mm = new MarketingMaterials();
	NDiraDepositories ndd = new NDiraDepositories();
	Sponsorloginlogout sl = new Sponsorloginlogout();
	Sponsorlogout slog = new Sponsorlogout();
	SponsorCompanyProfileUpdate spe = new SponsorCompanyProfileUpdate();
	SponsorAddInvestiment sai = new SponsorAddInvestiment();
	SponsorCurrentInvestments scid = new SponsorCurrentInvestments();
	UserSignUpwithSponser uss = new UserSignUpwithSponser();
	SponsorAddTeamMember sat = new SponsorAddTeamMember();
	SponsorManageTeamResetPassword smrp = new SponsorManageTeamResetPassword();
	SponsorManageTeamRemoveTeamMember smrt = new SponsorManageTeamRemoveTeamMember();
	SponsorDepositories sld = new SponsorDepositories();
	SponsorMessageToNDira smsg = new SponsorMessageToNDira();
	SponsorMyInfoUpdate sci = new SponsorMyInfoUpdate();
	NdiraLoginUsers nu = new NdiraLoginUsers();
	SponsorSignupMarketingMaterials smm = new SponsorSignupMarketingMaterials();
	SponsorSignupWebinarVideos swv = new SponsorSignupWebinarVideos();
	SponsorSignupFAQs sFAQs = new SponsorSignupFAQs();
	NDIRALoginDidYouKnow ndk = new NDIRALoginDidYouKnow();
	NDIRALoginDidYouKnowDelete ndkd = new NDIRALoginDidYouKnowDelete();
	NDIRALoginMarketingMaterials nmm = new NDIRALoginMarketingMaterials();
	NDIRALoginWebinars nlw = new NDIRALoginWebinars();
	NDiraloginFeeSchedules nfs = new NDiraloginFeeSchedules();
	NDiraloginDepositories nld = new NDiraloginDepositories();
	ForgetPassword fp = new ForgetPassword();
	InvalidLogin il = new InvalidLogin();
	InvalidSponsorSignup isu = new InvalidSponsorSignup();
	NDIRALoginSponsorUserNegative nsun = new NDIRALoginSponsorUserNegative();
	NDIRALoginRemoveTeammemberNegative nrtn = new NDIRALoginRemoveTeammemberNegative();
	NDIRAMessageNegative nmn = new NDIRAMessageNegative();
	NDIRAMarketingMaterialsNegative nmmn = new NDIRAMarketingMaterialsNegative();
	NDIRAUsersignupNegativeStep1 nusn = new NDIRAUsersignupNegativeStep1();
	NDIRAUsersignupNegativeStep2 nusn2 = new NDIRAUsersignupNegativeStep2();
	NDIRAUsersignupNegativeStep3 nusn3 = new NDIRAUsersignupNegativeStep3();
	SponsorRegularLoginLogout srlo = new SponsorRegularLoginLogout();
	ForgetPasswordNegative fpn = new ForgetPasswordNegative();
	AddInvestmentNegative ain = new AddInvestmentNegative();
	ManageTeamNegative mtn = new ManageTeamNegative();
	ManageRemoveTeamMemberNegative mremn = new ManageRemoveTeamMemberNegative();
	SponsorMessageNegative smn = new SponsorMessageNegative();
	SponsorUsersignupNegativeStep1 sun1 = new SponsorUsersignupNegativeStep1();
	SponsorUsersignupNegativeStep2 sun2 = new SponsorUsersignupNegativeStep2();
	SponsorUsersignupNegativeStep3 sun3 = new SponsorUsersignupNegativeStep3();
	NDIRALoginAdobeSignWidget nla = new NDIRALoginAdobeSignWidget();
	SponsorChangePassword scp=new SponsorChangePassword();
	SponsorChangePasswordNegative scpn=new SponsorChangePasswordNegative();
	SponsorInvestimentStatus sis=new SponsorInvestimentStatus();
	NDIRAWire_Instructions nwi=new NDIRAWire_Instructions();
	NDIRAHowDidYouKnow nhowdidyouknow=new NDIRAHowDidYouKnow();
	NDTCOUsers NDTCOUsers=new NDTCOUsers();
	NDTCOResetPassword NDTCORP=new NDTCOResetPassword();
	NDTCORemove NDTCORM=new NDTCORemove();
	DeleteSponsor ds=new DeleteSponsor();
	NDIRAWire_Instructions_Negative NDIRAWIN=new NDIRAWire_Instructions_Negative();
	NDTCOUsersNegative nun=new NDTCOUsersNegative();
	SponsorWireInstructions swi=new SponsorWireInstructions();
	AllocationsValidation av=new AllocationsValidation();
	InvestmentComponentsSave ics=new InvestmentComponentsSave();
	InvestmentComponentsClientRelationship iccr=new InvestmentComponentsClientRelationship();
	InvestmentComponentsSetAllocation icsa=new InvestmentComponentsSetAllocation();
	InvestmentComponentsAllAvailableFunds icaf=new InvestmentComponentsAllAvailableFunds();
	InvestComponentsDepositoryStorageOptions icso=new InvestComponentsDepositoryStorageOptions();
	InvestComponentsEnableTransactionFee ictf=new InvestComponentsEnableTransactionFee();
	InvestmentComponentsCourtesycall iccc=new InvestmentComponentsCourtesycall();
	InvestmentComponentsInvestmentTermsandConditions ictc=new InvestmentComponentsInvestmentTermsandConditions();
	ClientRelationshipforSponsorRelationshipAdvisor cfsr=new ClientRelationshipforSponsorRelationshipAdvisor();
	ClientRelationshipforSponsorrelationshipReferrer cfsrr=new ClientRelationshipforSponsorrelationshipReferrer();
	ClientRelationshipforSponsorRelationshipprovider csrp=new ClientRelationshipforSponsorRelationshipprovider();
	InvestmentComponentsdefaultforAdmin icda=new InvestmentComponentsdefaultforAdmin();
	InvestmentComponentsdefaultforReferrer icdr=new InvestmentComponentsdefaultforReferrer();
	InvestmentComponentsdefaultforprovider icdp=new InvestmentComponentsdefaultforprovider();
	ClientSignupStep3headingforAdvisor csha=new ClientSignupStep3headingforAdvisor();
	ClientSignupStep3headingforReferrer cshr=new ClientSignupStep3headingforReferrer();
	AddingDepositories ad=new AddingDepositories();
	Prepayment pp=new Prepayment();
	AccountOpeningPaymentClient client=new AccountOpeningPaymentClient();
	Sitedata_FeeSchedule_Update feeupdate=new Sitedata_FeeSchedule_Update();
	DepositoryInformationInStep3Validation dinf=new DepositoryInformationInStep3Validation();
	AddFeeScheduleinFees afs=new AddFeeScheduleinFees();
	FeeScheduleNAmeDisplayinginOpenAccount fso=new FeeScheduleNAmeDisplayinginOpenAccount();
	FeesValidation fv=new FeesValidation();
	Sitedata_Add_Fee_details afd=new Sitedata_Add_Fee_details();
	
	
	
	@Test
	public void NdiraTestV2() throws IOException 
	{
		tccount = xl.getRowCount(xlfile, tcsheet);
		tscount = xl.getRowCount(xlfile, tssheet);
		String finalsponsor=xl.getCellData(xlfile, tssheet, 1, 5);
		for (int i = 1; i <= tccount; i++)
		{
			tcexe = xl.getCellData(xlfile, tcsheet, i, 2);
			if (tcexe.equalsIgnoreCase("Y"))
			{
				tcid = xl.getCellData(xlfile, tcsheet, i, 0);
				for (int j = 1; j <= tscount; j++) 
				{
					try
					{
						tsid = xl.getCellData(xlfile, tssheet, j, 0);
					} 
					catch (Exception e) 
					{
						System.out.println("");
					}
					if (tcid.equalsIgnoreCase(tsid)) 
					{
						keyword = xl.getCellData(xlfile, tssheet, j, 4);

						switch (keyword.toUpperCase()) 
						{
						
						
						case "NDIRALOGIN":
							nd.suname = xl.getCellData(xlfile, tssheet, j, 5);
							nd.spwd = xl.getCellData(xlfile, tssheet, j, 6);
							res = nd.Login();
							break;

												
						case "USERSIGNUPWITHSPONSER":
							res = usp.ClientSignUp();
							break;

						case "SPONSERSIGNUP":
							sup.company = xl.getCellData(xlfile, tssheet, j, 5);
							sup.Category = xl.getCellData(xlfile, tssheet, j, 6);
							sup.fname = xl.getCellData(xlfile, tssheet, j, 7);
							sup.lname = xl.getCellData(xlfile, tssheet, j, 8);
							sup.email = xl.getCellData(xlfile, tssheet, j, 9);
							sup.number = xl.getCellData(xlfile, tssheet, j, 10);
							sup.Howdiduknow = xl.getCellData(xlfile, tssheet, j, 11);
							sup.details = xl.getCellData(xlfile, tssheet, j, 12);
							sup.password = xl.getCellData(xlfile, tssheet, j, 13);
							sup.cpass = xl.getCellData(xlfile, tssheet, j, 14);
							res = sup.sponserSignupTest();
							Sleeper.sleepTightInSeconds(2);
							break;

						case "COMPANYEDIT":
							ce.sponsor=finalsponsor;
							ce.saddress1 = xl.getCellData(xlfile, tssheet, j, 5);
							ce.saddress2 = xl.getCellData(xlfile, tssheet, j, 6);
							ce.scity = xl.getCellData(xlfile, tssheet, j, 7);
							ce.szipcode = xl.getCellData(xlfile, tssheet, j, 9);
							ce.state=xl.getCellData(xlfile, tssheet, j, 8);							
							ce.website=xl.getCellData(xlfile, tssheet, j, 10);							
							ce.p1=xl.getCellData(xlfile, tssheet, j, 11);							
							ce.p2=xl.getCellData(xlfile, tssheet, j, 12);
							res = ce.companyEdit(ce.saddress1, ce.saddress2, ce.scity, ce.szipcode,ce.state,ce.website,ce.p1,ce.p2);
							d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
							out.Logout();
							break;

						case "USERSIGNUPWITHNDIRA":
							usnl.fname = xl.getCellData(xlfile, tssheet, j, 5);
							usnl.lname = xl.getCellData(xlfile, tssheet, j, 6);
							usnl.email = xl.getCellData(xlfile, tssheet, j, 7);
							usnl.pass = xl.getCellData(xlfile, tssheet, j, 8);
							usnl.cpass = xl.getCellData(xlfile, tssheet, j, 9);
							usnl.DOB = xl.getCellData(xlfile, tssheet, j, 10);
							usnl.SSN = xl.getCellData(xlfile, tssheet, j, 11);
							usnl.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);
							usnl.phone = xl.getCellData(xlfile, tssheet, j, 13);
							usnl.address1 = xl.getCellData(xlfile, tssheet, j, 14);
							usnl.address2 = xl.getCellData(xlfile, tssheet, j, 15);
							usnl.city = xl.getCellData(xlfile, tssheet, j, 16);
							usnl.state = xl.getCellData(xlfile, tssheet, j, 17);
							usnl.zipcode = xl.getCellData(xlfile, tssheet, j, 18);
							usnl.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);
							usnl.investiment = xl.getCellData(xlfile, tssheet, j, 20);
							usnl.phonen = xl.getCellData(xlfile, tssheet, j, 21);
							usnl.cardno = xl.getCellData(xlfile, tssheet, j, 22);
							usnl.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 23);
							usnl.cardName = xl.getCellData(xlfile, tssheet, j, 24);
							usnl.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 25);
							res = usnl.UserSignupwithNDiralogin();
							//xl.setCellData(xlfile, tssheet, j, 26, UserSignupwithNDiralogin.AccNo);
							break;

						case "COMPANYOVERVIEW":
							co.sponsor=finalsponsor;
							co.sponsorrelationship = xl.getCellData(xlfile, tssheet, j, 5);
							co.picategory = xl.getCellData(xlfile, tssheet, j, 6);
							co.type = xl.getCellData(xlfile, tssheet, j, 7);
							co.Howdidyouhear = xl.getCellData(xlfile, tssheet, j, 8);							
							co.OverrideFeeBuffer=xl.getCellData(xlfile, tssheet, j, 9);
							co.expiry = xl.getCellData(xlfile, tssheet, j, 10);
							co.min=xl.getCellData(xlfile, tssheet, j, 11);
							co.max=xl.getCellData(xlfile, tssheet, j, 12);
							co.amount=xl.getCellData(xlfile, tssheet, j, 13);
							res = co.companyOverview();
							out.Logout();
							break;

						case "SPONSORMESSAGE":
							sm.message = xl.getCellData(xlfile, tssheet, j, 5);
							res = sm.sponsermessages();
							out.Logout();
							break;

						case "INVESTMENTS":
							res = si.SponsorInvestiments();
							out.Logout();
							break;

						case "FEES":
							res = sf.SponsorFees();
							out.Logout();
							break;

						case "NDIRAADDTEAMMEMBER":
							su.fname = xl.getCellData(xlfile, tssheet, j, 5);
							su.lname = xl.getCellData(xlfile, tssheet, j, 6);
							su.email = xl.getCellData(xlfile, tssheet, j, 7);
							su.role=xl.getCellData(xlfile, tssheet, j, 8);
							res = su.NDiraAddSponsorUser();
							out.Logout();
							break;

						case "SPONSORUSERSREMOVE":
							res = sremove.Sponsoruserremove();
							out.Logout();
							break;

						case "SPONSORUSERSRESETPASSWORD":
							res = srp.SponsorUserResetpassword();
							out.Logout();
							break;

						case "MARKETINGMATERIALS":
							mm.Title = xl.getCellData(xlfile, tssheet, j, 5);
							mm.Desc = xl.getCellData(xlfile, tssheet, j, 6);
							res = mm.MarketingMaterials();
							out.Logout();
							break;

						case "ADOBESIGNWIDGET":
							res = nla.NDIRALoginAdobeSignWidget();
							out.Logout();
							break;

						case "NDIRADEPOSITORIES":
							res = ndd.Ndiradepositories();
							out.Logout();
							break;

						case "SPONSORLOGINLOGOUT":
							sl.email = xl.getCellData(xlfile, tssheet, 1, 9);
							sl.password = xl.getCellData(xlfile, tssheet, 1, 13);
							res = sl.Login(sl.email, sl.password);
							break;

						case "SPONSORCOMPANYPROFILE":
							spe.Address1 = xl.getCellData(xlfile, tssheet, j, 5);
							spe.Address2 = xl.getCellData(xlfile, tssheet, j, 6);
							spe.city = xl.getCellData(xlfile, tssheet, j, 7);
							spe.state1 = xl.getCellData(xlfile, tssheet, j, 8);
							spe.Zipcode = xl.getCellData(xlfile, tssheet, j, 9);
							spe.website = xl.getCellData(xlfile, tssheet, j, 10);
							spe.Phone1 = xl.getCellData(xlfile, tssheet, j, 11);
							spe.Phone2 = xl.getCellData(xlfile, tssheet, j, 12);
							res = spe.SponsorCompanyProfileUpdate();
							slog.Logout();

							break;

						case "SPONSORADDINVESTMENT":
							sai.name = xl.getCellData(xlfile, tssheet, j, 5);
							sai.Investment = xl.getCellData(xlfile, tssheet, j, 6);
							sai.text = xl.getCellData(xlfile, tssheet, j, 7);
							res = sai.SponsorAddInvestment();
							slog.Logout();

							break;

						case "CURRENTINVESTMENTDELETE":
							res = scid.SponsorCurrentInvestigations();
							slog.Logout();
							break;

						case "USERSIGNUPWITHSPONSOR":
							uss.fname = xl.getCellData(xlfile, tssheet, j, 5);
							uss.lname = xl.getCellData(xlfile, tssheet, j, 6);
							uss.email = xl.getCellData(xlfile, tssheet, j, 7);
							uss.pass = xl.getCellData(xlfile, tssheet, j, 8);
							uss.cpass = xl.getCellData(xlfile, tssheet, j, 9);
							uss.DOB = xl.getCellData(xlfile, tssheet, j, 10);
							uss.SSN = xl.getCellData(xlfile, tssheet, j, 11);
							uss.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);
							uss.phone = xl.getCellData(xlfile, tssheet, j, 13);
							uss.address1 = xl.getCellData(xlfile, tssheet, j, 14);
							uss.address2 = xl.getCellData(xlfile, tssheet, j, 15);
							uss.city = xl.getCellData(xlfile, tssheet, j, 16);
							uss.state = xl.getCellData(xlfile, tssheet, j, 17);
							uss.zipcode = xl.getCellData(xlfile, tssheet, j, 18);
							uss.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);
							uss.investiment = xl.getCellData(xlfile, tssheet, j, 20);
							uss.phonen = xl.getCellData(xlfile, tssheet, j, 21);
							uss.cardno = xl.getCellData(xlfile, tssheet, j, 22);
							uss.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 23);
							uss.cardName = xl.getCellData(xlfile, tssheet, j, 24);
							uss.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 25);
							res = uss.ClientSignUp();							
							d.get(url);
							break;

						case "SPONSORADDTEAMMEMBER":
							sat.fname = xl.getCellData(xlfile, tssheet, j, 5);
							sat.lname = xl.getCellData(xlfile, tssheet, j, 6);
							sat.email = xl.getCellData(xlfile, tssheet, j, 7);
							sat.role = xl.getCellData(xlfile, tssheet, j, 8);
							res = sat.SponsorAddTeamMember();
							slog.Logout();
							break;

						case "SPONSORRESETPASSWORD":
							res = smrp.SponsorManageTeamResetPassword();
							slog.Logout();
							break;

						case "SPONSORREMOVETEAMMEMBER":
							res = smrt.RemoveTeamMember();
							slog.Logout();
							break;
							
						case "SPONSORDEPOSITORIES":
							res = sld.SponsorDepositories();
							slog.Logout();
							break;

						case "SPONSORMESSAGETONDIRA":
							smsg.text = xl.getCellData(xlfile, tssheet, j, 5);
							res = smsg.SponsorMessageToNDira();
							slog.Logout();
							break;

						case "MYINFOUPDATE":
							sci.fname = xl.getCellData(xlfile, tssheet, j, 5);
							sci.lname = xl.getCellData(xlfile, tssheet, j, 6);
							sci.email = xl.getCellData(xlfile, tssheet, j, 7);							
							res = sci.SponsorMyInfoUpdate();
							xl.setCellData(xlfile, tssheet, 1, 9, sci.email);
							slog.Logout();
							break;

						case "NDIRAUSERS":
							res = nu.NdiraLoginUsers();
							out.Logout();
							break;

						case "SPONSORMARKETINGMATERIALS":
							res = smm.SponsorSignupMarketingMaterials();
							slog.Logout();
							break;

						case "SPONSORWEBINARVIDEO":
							res = swv.SponsorSignupWebinarVideo();
							slog.Logout();
							break;

						case "SPONSORFAQS":
							res = sFAQs.SponsorSignupFAQs();
							slog.Logout();
							break;

						case "NDIRADIDYOUKNOW":
							ndk.subject = xl.getCellData(xlfile, tssheet, j, 5);
							res = ndk.NDiraDidYouKnow();
							out.Logout();
							break;

						case "NDIRADIDYOUKNOWDELETE":
							res = ndkd.NDIRALoginDidYouKnowDelete();
							out.Logout();
							break;

						case "NDIRAMARKETINGMATERIALS":
							res = nmm.NDIRALoginMarketingMaterials();
							out.Logout();
							break;

						case "NDIRAWEBINARS":
							res = nlw.NDIRALoginWebinars();
							out.Logout();
							break;

						case "NDIRASITEDATAFEESCHEDULES":
							nfs.name=xl.getCellData(xlfile, tssheet, j, 5);
							nfs.displayname=xl.getCellData(xlfile, tssheet, j, 6);							
							nfs.oraclecode=xl.getCellData(xlfile, tssheet, j, 7);
							nfs.feeschedulename=xl.getCellData(xlfile, tssheet, j, 8);
							nfs.Displaytext=xl.getCellData(xlfile, tssheet, j, 9);
							res = nfs.NDiraloginFeeSchedules();
							out.Logout();
							break;

						case "NDIRASITEDATADEPOSITORIES":
							res = nld.NDiraloginDepositories();
							out.Logout();
							break;

						case "FORGETPASSWORD":
							fp.email = xl.getCellData(xlfile, tssheet, j, 5);
							res = fp.Forgetpassword();
							break;

						case "INVALIDLOGIN":
							il.uname = xl.getCellData(xlfile, tssheet, j, 5);
							il.pass = xl.getCellData(xlfile, tssheet, j, 6);
							res = il.InvalidLogin();
							d.get(url);
							break;

						case "INVALIDLOGIN1":
							il.uname = xl.getCellData(xlfile, tssheet, j, 5);
							il.pass = xl.getCellData(xlfile, tssheet, j, 6);
							res = il.InvalidLogin();
							d.get(url);
							break;

						case "INVALIDLOGIN2":
							il.uname = xl.getCellData(xlfile, tssheet, j, 5);
							il.pass = xl.getCellData(xlfile, tssheet, j, 6);
							res = il.InvalidLogin();
							d.get(url);
							break;

						case "INVALIDLOGIN3":
							il.uname = xl.getCellData(xlfile, tssheet, j, 5);
							il.pass = xl.getCellData(xlfile, tssheet, j, 6);
							res = il.InvalidLogin();
							d.get(url);
							break;

						case "INVALIDLOGIN4":
							il.uname = xl.getCellData(xlfile, tssheet, j, 5);
							il.pass = xl.getCellData(xlfile, tssheet, j, 6);
							res = il.InvalidLogin();
							d.get(url);
							break;

						case "INVALIDLOGIN5":
							il.uname = xl.getCellData(xlfile, tssheet, j, 5);
							il.pass = xl.getCellData(xlfile, tssheet, j, 6);
							res = il.InvalidLogin();
							d.get(url);
							break;

						case "INVALIDSPONSORSIGNUP":
							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);
							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.Howdiduknow = xl.getCellData(xlfile, tssheet, j, 11);
							isu.details = xl.getCellData(xlfile, tssheet, j, 12);
							isu.password = xl.getCellData(xlfile, tssheet, j, 13);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 14);
							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP1":
							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);
							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.Howdiduknow = xl.getCellData(xlfile, tssheet, j, 11);
							isu.details = xl.getCellData(xlfile, tssheet, j, 12);
							isu.password = xl.getCellData(xlfile, tssheet, j, 13);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 14);

							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP2":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);
							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.Howdiduknow = xl.getCellData(xlfile, tssheet, j, 11);
							isu.details = xl.getCellData(xlfile, tssheet, j, 12);
							isu.password = xl.getCellData(xlfile, tssheet, j, 13);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 14);

							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP3":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);
							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.Howdiduknow = xl.getCellData(xlfile, tssheet, j, 11);
							isu.details = xl.getCellData(xlfile, tssheet, j, 12);
							isu.password = xl.getCellData(xlfile, tssheet, j, 13);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 14);
							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP4":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);
							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.Howdiduknow = xl.getCellData(xlfile, tssheet, j, 11);
							isu.details = xl.getCellData(xlfile, tssheet, j, 12);
							isu.password = xl.getCellData(xlfile, tssheet, j, 13);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 14);

							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP5":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);
							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.Howdiduknow = xl.getCellData(xlfile, tssheet, j, 11);
							isu.details = xl.getCellData(xlfile, tssheet, j, 12);
							isu.password = xl.getCellData(xlfile, tssheet, j, 13);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 14);
							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP6":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);
							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.Howdiduknow = xl.getCellData(xlfile, tssheet, j, 11);
							isu.details = xl.getCellData(xlfile, tssheet, j, 12);
							isu.password = xl.getCellData(xlfile, tssheet, j, 13);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 14);
							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP7":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);
							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.Howdiduknow = xl.getCellData(xlfile, tssheet, j, 11);
							isu.details = xl.getCellData(xlfile, tssheet, j, 12);
							isu.password = xl.getCellData(xlfile, tssheet, j, 13);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 14);

							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP8":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);
							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.Howdiduknow = xl.getCellData(xlfile, tssheet, j, 11);
							isu.details = xl.getCellData(xlfile, tssheet, j, 12);
							isu.password = xl.getCellData(xlfile, tssheet, j, 13);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 14);

							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP9":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);
							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.Howdiduknow = xl.getCellData(xlfile, tssheet, j, 11);
							isu.details = xl.getCellData(xlfile, tssheet, j, 12);
							isu.password = xl.getCellData(xlfile, tssheet, j, 13);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 14);

							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP10":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);
							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.Howdiduknow = xl.getCellData(xlfile, tssheet, j, 11);
							isu.details = xl.getCellData(xlfile, tssheet, j, 12);
							isu.password = xl.getCellData(xlfile, tssheet, j, 13);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 14);

							res = isu.InvalidSponsorSignup();
							break;

						case "NDIRASPONSORUSERNEGATIVE":
							nsun.uname = xl.getCellData(xlfile, tssheet, j, 5);
							nsun.pass = xl.getCellData(xlfile, tssheet, j, 6);
							nsun.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = nsun.NDIRALoginSponsorUserNegative();
							Sleeper.sleepTightInSeconds(2);
							out.Logout();
							break;

						case "NDIRASPONSORUSERNEGATIVE1":
							nsun.uname = xl.getCellData(xlfile, tssheet, j, 5);
							nsun.pass = xl.getCellData(xlfile, tssheet, j, 6);
							nsun.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = nsun.NDIRALoginSponsorUserNegative();
							out.Logout();
							break;

						case "NDIRASPONSORUSERNEGATIVE2":
							nsun.uname = xl.getCellData(xlfile, tssheet, j, 5);
							nsun.pass = xl.getCellData(xlfile, tssheet, j, 6);
							nsun.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = nsun.NDIRALoginSponsorUserNegative();
							out.Logout();
							break;

						case "NDIRASPONSORUSERNEGATIVE3":
							nsun.uname = xl.getCellData(xlfile, tssheet, j, 5);
							nsun.pass = xl.getCellData(xlfile, tssheet, j, 6);
							nsun.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = nsun.NDIRALoginSponsorUserNegative();
							out.Logout();
							break;

						case "NDIRASPONSORUSERNEGATIVE4":
							nsun.uname = xl.getCellData(xlfile, tssheet, j, 5);
							nsun.pass = xl.getCellData(xlfile, tssheet, j, 6);
							nsun.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = nsun.NDIRALoginSponsorUserNegative();
							out.Logout();
							break;

						case "NDIRAREMOVETEAMMEMBERNEGATIVE":
							nrtn.uname = xl.getCellData(xlfile, tssheet, j, 5);
							nrtn.pass = xl.getCellData(xlfile, tssheet, j, 6);
							nrtn.email = xl.getCellData(xlfile, tssheet, j, 7);
							nrtn.cemail = xl.getCellData(xlfile, tssheet, j, 8);
							res = nrtn.NDIRALoginRemoveTeammemberNegative();
							out.Logout();
							break;

						case "NDIRAMESSAGENEGATIVE":
							res = nmn.NDIRAMessageNegative();
							out.Logout();
							break;

						case "NDIRAMARKETINGMATERIALSNEGATIVE":
							res = nmmn.NDIRAMarketingMaterialsNegative();
							out.Logout();
							break;

						case "NDIRAUSERSIGNUPNEGATIVE":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);
							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);
							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);
							res = nusn.NDIRAUsersignupNegative();
							String rese = d.findElement(By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div/div[1]/label")).getText();
							//System.out.println(rese);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();
							break;

						case "NDIRAUSERSIGNUPNEGATIVE1":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);
							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);
							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);
							res = nusn.NDIRAUsersignupNegative();

							String rese1 = d.findElement(By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div/div[3]/label")).getText();
							System.out.println(rese1);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE2":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);
							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);
							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = nusn.NDIRAUsersignupNegative();
							String rese2 = d.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[1]")).getText();
							//System.out.println(rese2);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE3":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);
							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);
							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);
							res = nusn.NDIRAUsersignupNegative();
							String rese3 = d.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[1]")).getText();
							//System.out.println(rese3);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE4":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = nusn.NDIRAUsersignupNegative();
							String rese4 = d
									.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[2]"))
									.getText();
							//System.out.println(rese4);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE5":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = nusn.NDIRAUsersignupNegative();
							String rese5 = d
									.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[2]"))
									.getText();
							//System.out.println(rese5);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE6":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = nusn.NDIRAUsersignupNegative();
							Sleeper.sleepTightInSeconds(5);
							String rese6 = d.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label"))
									.getText();
							//System.out.println(rese6);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE7":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = nusn.NDIRAUsersignupNegative();

							Sleeper.sleepTightInSeconds(8);
							String rese7 = d.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(rese7);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE8":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese8 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(rese8);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE9":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese9 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(rese9);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE10":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese10 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(rese10);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE11":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese11 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[2]/label"))
									.getText();
							System.out.println(rese11);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE12":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese12 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[2]/label"))
									.getText();
							System.out.println(rese12);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE14":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese14 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(rese14);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE15":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese15 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(rese15);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE16":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese16 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(rese16);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE17":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese17 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[2]/div[1]/label"))
									.getText();
							System.out.println(rese17);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE19":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese19 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[2]/div[3]/label"))
									.getText();
							System.out.println(rese19);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE20":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese20 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[2]/div[3]/label"))
									.getText();
							System.out.println(rese20);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE21":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese21 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[2]/div[3]/label"))
									.getText();
							System.out.println(rese21);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE22":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							nusn3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							nusn3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							nusn3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							nusn3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = nusn3.NDIRAUsersignupNegativeStep3();
							String rese22 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(rese22);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE23":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							nusn3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							nusn3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							nusn3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							nusn3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = nusn3.NDIRAUsersignupNegativeStep3();
							String rese23 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(rese23);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE24":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							nusn3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							nusn3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							nusn3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							nusn3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = nusn3.NDIRAUsersignupNegativeStep3();
							String rese24 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[1]/div[2]/label"))
									.getText();
							System.out.println(rese24);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE25":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							nusn3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							nusn3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							nusn3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							nusn3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = nusn3.NDIRAUsersignupNegativeStep3();
							String rese25 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[1]/div[2]/label"))
									.getText();
							System.out.println(rese25);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE26":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							nusn3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							nusn3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							nusn3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							nusn3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = nusn3.NDIRAUsersignupNegativeStep3();
							String rese26 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[2]/div[1]/label"))
									.getText();
							System.out.println(rese26);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE27":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							nusn3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							nusn3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							nusn3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							nusn3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = nusn3.NDIRAUsersignupNegativeStep3();
							String rese27 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(rese27);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE28":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							nusn3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							nusn3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							nusn3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							nusn3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = nusn3.NDIRAUsersignupNegativeStep3();
							String rese28 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(rese28);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();
							break;

						case "SPONSORREGULARLOGINLOGOUT":
							srlo.uname = xl.getCellData(xlfile, tssheet, j, 5);
							srlo.pass = xl.getCellData(xlfile, tssheet, j, 6);
							res = srlo.SponsorRegularLoginLogout();
							break;

						case "FORGETPASSWORDNEGATIVE":
							fpn.email = xl.getCellData(xlfile, tssheet, j, 5);
							res = fpn.ForgetPasswordNegative();
							d.get(url);
							break;

						case "ADDINVESTMENTNEGATIVE":
							ain.Investment = xl.getCellData(xlfile, tssheet, j, 5);
							ain.Type = xl.getCellData(xlfile, tssheet, j, 6);
							ain.Description = xl.getCellData(xlfile, tssheet, j, 7);
							res = ain.AddInvestmentNegative();
							slog.Logout();
							break;

						case "ADDINVESTMENTNEGATIVE1":
							ain.Investment = xl.getCellData(xlfile, tssheet, j, 5);
							ain.Type = xl.getCellData(xlfile, tssheet, j, 6);
							ain.Description = xl.getCellData(xlfile, tssheet, j, 7);
							res = ain.AddInvestmentNegative();
							slog.Logout();
							break;

						case "ADDINVESTMENTNEGATIVE2":
							ain.Investment = xl.getCellData(xlfile, tssheet, j, 5);
							ain.Type = xl.getCellData(xlfile, tssheet, j, 6);
							ain.Description = xl.getCellData(xlfile, tssheet, j, 7);
							res = ain.AddInvestmentNegative();
							slog.Logout();
							break;

						case "ADDINVESTMENTNEGATIVE3":
							ain.Investment = xl.getCellData(xlfile, tssheet, j, 5);
							ain.Type = xl.getCellData(xlfile, tssheet, j, 6);
							ain.Description = xl.getCellData(xlfile, tssheet, j, 7);
							res = ain.AddInvestmentNegative();
							slog.Logout();
							break;

						case "MANAGETEAMNEGATIVE":
							mtn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							mtn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							mtn.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = mtn.ManageTeamNegative();
							slog.Logout();
							break;

						case "MANAGETEAMNEGATIVE1":
							mtn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							mtn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							mtn.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = mtn.ManageTeamNegative();
							slog.Logout();
							break;

						case "MANAGETEAMNEGATIVE2":
							mtn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							mtn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							mtn.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = mtn.ManageTeamNegative();
							slog.Logout();
							break;

						case "MANAGETEAMNEGATIVE3":
							mtn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							mtn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							mtn.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = mtn.ManageTeamNegative();
							slog.Logout();
							break;

						case "MANAGETEAMNEGATIVE4":
							mtn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							mtn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							mtn.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = mtn.ManageTeamNegative();
							slog.Logout();
							break;

						case "MANAGETEAMNEGATIVE5":
							mtn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							mtn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							mtn.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = mtn.ManageTeamNegative();
							slog.Logout();
							break;

						case "MANAGEREMOVETEAMMEMBERNEGATIVE":
							mremn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							mremn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							mremn.email = xl.getCellData(xlfile, tssheet, j, 7);
							mremn.email1 = xl.getCellData(xlfile, tssheet, j, 8);
							res = mremn.ManageRemoveTeamMemberNegative();
							slog.Logout();
							break;

						case "SPONSORMESSAGENEGATIVE":
							res = smn.SponsorMessageNegative();
							slog.Logout();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = sun1.SponsorUsersignupNegativeStep1();

							String Srese = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div/div[1]/label"))
									.getText();
							System.out.println(Srese);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE1":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = sun1.SponsorUsersignupNegativeStep1();

							String Srese1 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div/div[3]/label"))
									.getText();
							System.out.println(Srese1);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE2":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = sun1.SponsorUsersignupNegativeStep1();

							String Srese2 = d
									.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[1]"))
									.getText();
							System.out.println(Srese2);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE3":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = sun1.SponsorUsersignupNegativeStep1();

							String Srese3 = d
									.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[1]"))
									.getText();
							System.out.println(Srese3);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE4":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = sun1.SponsorUsersignupNegativeStep1();

							String Srese4 = d
									.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[2]"))
									.getText();
							System.out.println(Srese4);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE5":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = sun1.SponsorUsersignupNegativeStep1();

							String Srese5 = d
									.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[2]"))
									.getText();
							System.out.println(Srese5);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE6":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = sun1.SponsorUsersignupNegativeStep1();

							String Srese6 = d
									.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[3]"))
									.getText();
							System.out.println(Srese6);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE7":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = sun1.SponsorUsersignupNegativeStep1();

							Sleeper.sleepTightInSeconds(8);

							String Srese7 = d.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(Srese7);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE8":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese8 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(Srese8);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE9":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese9 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(Srese9);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE10":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese10 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(Srese10);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE11":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese11 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[2]/label"))
									.getText();
							System.out.println(Srese11);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE12":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese12 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[2]/label"))
									.getText();
							System.out.println(Srese12);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE14":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese14 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(Srese14);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE15":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese15 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(Srese15);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE16":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese16 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(Srese16);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE17":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese17 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[2]/div[1]/label"))
									.getText();
							System.out.println(Srese17);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE19":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese19 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[2]/div[3]/label"))
									.getText();
							System.out.println(Srese19);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE20":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese20 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[2]/div[3]/label"))
									.getText();
							System.out.println(Srese20);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE21":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese21 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[2]/div[3]/label"))
									.getText();
							System.out.println(Srese21);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE22":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							sun3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							sun3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							sun3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							sun3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = sun3.SponsorUsersignupNegativeStep3();
							String Srese22 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(Srese22);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE23":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							sun3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							sun3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							sun3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							sun3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = sun3.SponsorUsersignupNegativeStep3();
							String Srese23 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(Srese23);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE24":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							sun3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							sun3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							sun3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							sun3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = sun3.SponsorUsersignupNegativeStep3();
							String Srese24 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[1]/div[2]/label"))
									.getText();
							System.out.println(Srese24);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE25":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							sun3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							sun3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							sun3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							sun3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = sun3.SponsorUsersignupNegativeStep3();
							String Srese25 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[1]/div[2]/label"))
									.getText();
							System.out.println(Srese25);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE26":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							sun3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							sun3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							sun3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							sun3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = sun3.SponsorUsersignupNegativeStep3();
							String Srese26 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[2]/div[1]/label"))
									.getText();
							System.out.println(Srese26);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE27":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							sun3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							sun3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							sun3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							sun3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = sun3.SponsorUsersignupNegativeStep3();
							String Srese27 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(Srese27);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE28":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							sun3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							sun3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							sun3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							sun3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = sun3.SponsorUsersignupNegativeStep3();
							String Srese28 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(Srese28);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;
							
						case "SPONSORCHANGEPASSWORD":
							scp.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scp.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scp.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scp.SponsorChangePassword();
							slog.Logout();
							break;
							
						case "SPONSORCHANGEPASSWORDNEGATIVE":
							scpn.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scpn.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scpn.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scpn.SponsorChangePasswordNegative();
							slog.Logout();
							//System.out.println("SPONSORCHANGEPASSWORDNEGATIVE pass");
							break;
							
						case "SPONSORCHANGEPASSWORDNEGATIVE1":
							scpn.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scpn.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scpn.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scpn.SponsorChangePasswordNegative();
							slog.Logout();
							break;
							
						case "SPONSORCHANGEPASSWORDNEGATIVE2":
							scpn.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scpn.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scpn.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scpn.SponsorChangePasswordNegative();
							slog.Logout();
							break;
							
						case "SPONSORCHANGEPASSWORDNEGATIVE3":
							scpn.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scpn.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scpn.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scpn.SponsorChangePasswordNegative();
							slog.Logout();
							break;
							
						case "SPONSORCHANGEPASSWORDNEGATIVE4":
							scpn.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scpn.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scpn.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scpn.SponsorChangePasswordNegative();
							slog.Logout();
							break;
							
						case "SPONSORCHANGEPASSWORDNEGATIVE5":
							scpn.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scpn.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scpn.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scpn.SponsorChangePasswordNegative();
							slog.Logout();
							break;
							
						case "SPONSORCHANGEPASSWORDNEGATIVE6":
							scpn.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scpn.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scpn.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scpn.SponsorChangePasswordNegative();
							slog.Logout();
							break;
							
						case "SPONSORCHANGEPASSWORDNEGATIVE7":
							scpn.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scpn.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scpn.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scpn.SponsorChangePasswordNegative();
							slog.Logout();
							break;
							
						case "SPONSORINVESTMENTSTATUS":
							sai.name = xl.getCellData(xlfile, tssheet, j, 5);
							sai.Investment = xl.getCellData(xlfile, tssheet, j, 6);
							sai.text = xl.getCellData(xlfile, tssheet, j, 7);
							sis.invstatus=xl.getCellData(xlfile, tssheet, j, 8);
							sis.filestatus=xl.getCellData(xlfile, tssheet, j, 9);
							res=sis.sponsorInvestmentStatus();
							break;
							
						case "SPONSORINVESTMENTFILESTATUS":
							res=sis.sponsorInvestmentDocumentStatus();
							slog.Logout();
							break;
							
						case "WIREINSTRUCTIONS":
							nwi.BankName = xl.getCellData(xlfile, tssheet, j, 5);
							nwi.Address = xl.getCellData(xlfile, tssheet, j, 6);
							nwi.City = xl.getCellData(xlfile, tssheet, j, 7);
							nwi.State = xl.getCellData(xlfile, tssheet, j, 8);
							nwi.Zip = xl.getCellData(xlfile, tssheet, j, 9);
							nwi.accname = xl.getCellData(xlfile, tssheet, j, 10);
							nwi.AccountNumber = xl.getCellData(xlfile, tssheet, j, 11);
							nwi.RoutingNumber = xl.getCellData(xlfile, tssheet, j, 12);
							res=nwi.NDIRAWire_InstructionsTest();
							out.Logout();
							break;	
							
						case "NDIRAHOWDIDYOUKNOW":							
							res = nhowdidyouknow.NDIRAHowDidYouKnow();
							out.Logout();
							break;
							
						case "ADDNDTCOUSERS":
							NDTCOUsers.fname=xl.getCellData(xlfile, tssheet, j, 5);
							NDTCOUsers.lname=xl.getCellData(xlfile, tssheet, j, 6);
							NDTCOUsers.email=xl.getCellData(xlfile, tssheet, j, 7);
							res = NDTCOUsers.NDTCOUsersTest();
							out.Logout();
							break;
							
						case "RPNDTCO":
							NDTCORP.fname=xl.getCellData(xlfile, tssheet, j, 5);
							NDTCORP.lname=xl.getCellData(xlfile, tssheet, j, 6);
							NDTCORP.email=xl.getCellData(xlfile, tssheet, j, 7);
							res = NDTCORP.NDTCOResetPasswordTest();
							out.Logout();
							break;
							
						case "NDTCOREMOVE":
							NDTCORM.fname=xl.getCellData(xlfile, tssheet, j, 5);
							NDTCORM.lname=xl.getCellData(xlfile, tssheet, j, 6);
							NDTCORM.email=xl.getCellData(xlfile, tssheet, j, 7);
							res = NDTCORM.NDTCORemoveUserTest();
							out.Logout();
							break;
							
						case "DELETESPONSOR":
							ds.company = xl.getCellData(xlfile, tssheet, j, 5);
							ds.Category = xl.getCellData(xlfile, tssheet, j, 6);
							ds.fname = xl.getCellData(xlfile, tssheet, j, 7);
							ds.lname = xl.getCellData(xlfile, tssheet, j, 8);
							ds.email = xl.getCellData(xlfile, tssheet, j, 9);
							ds.number = xl.getCellData(xlfile, tssheet, j, 10);
							ds.Howdiduknow = xl.getCellData(xlfile, tssheet, j, 11);
							ds.details = xl.getCellData(xlfile, tssheet, j, 12);
							ds.password = xl.getCellData(xlfile, tssheet, j, 13);
							ds.cpass = xl.getCellData(xlfile, tssheet, j, 14);
							res = ds.DeleteSponsorTest();							
							break;
						
						case "WIREINSTRUCTIONSNEGATIVE":
							NDIRAWIN.BankName = xl.getCellData(xlfile, tssheet, j, 5);
							NDIRAWIN.Address = xl.getCellData(xlfile, tssheet, j, 6);
							NDIRAWIN.City = xl.getCellData(xlfile, tssheet, j, 7);
							NDIRAWIN.State = xl.getCellData(xlfile, tssheet, j, 8);
							NDIRAWIN.Zip = xl.getCellData(xlfile, tssheet, j, 9);
							NDIRAWIN.accname = xl.getCellData(xlfile, tssheet, j, 10);
							NDIRAWIN.AccountNumber = xl.getCellData(xlfile, tssheet, j, 11);
							NDIRAWIN.RoutingNumber = xl.getCellData(xlfile, tssheet, j, 12);
							res=NDIRAWIN.NDIRAWire_Instructions_NegativeTest();
							out.Logout();
							break;	
							
						case "WIREINSTRUCTIONSNEGATIVE1":
							NDIRAWIN.BankName = xl.getCellData(xlfile, tssheet, j, 5);
							NDIRAWIN.Address = xl.getCellData(xlfile, tssheet, j, 6);
							NDIRAWIN.City = xl.getCellData(xlfile, tssheet, j, 7);
							NDIRAWIN.State = xl.getCellData(xlfile, tssheet, j, 8);
							NDIRAWIN.Zip = xl.getCellData(xlfile, tssheet, j, 9);
							NDIRAWIN.accname = xl.getCellData(xlfile, tssheet, j, 10);
							NDIRAWIN.AccountNumber = xl.getCellData(xlfile, tssheet, j, 11);
							NDIRAWIN.RoutingNumber = xl.getCellData(xlfile, tssheet, j, 12);
							res=NDIRAWIN.NDIRAWire_Instructions_NegativeTest();
							out.Logout();
							break;	
							
						case "WIREINSTRUCTIONSNEGATIVE2":
							NDIRAWIN.BankName = xl.getCellData(xlfile, tssheet, j, 5);
							NDIRAWIN.Address = xl.getCellData(xlfile, tssheet, j, 6);
							NDIRAWIN.City = xl.getCellData(xlfile, tssheet, j, 7);
							NDIRAWIN.State = xl.getCellData(xlfile, tssheet, j, 8);
							NDIRAWIN.Zip = xl.getCellData(xlfile, tssheet, j, 9);
							NDIRAWIN.accname = xl.getCellData(xlfile, tssheet, j, 10);
							NDIRAWIN.AccountNumber = xl.getCellData(xlfile, tssheet, j, 11);
							NDIRAWIN.RoutingNumber = xl.getCellData(xlfile, tssheet, j, 12);
							res=NDIRAWIN.NDIRAWire_Instructions_NegativeTest();
							out.Logout();
							break;	
							
						case "WIREINSTRUCTIONSNEGATIVE3":
							NDIRAWIN.BankName = xl.getCellData(xlfile, tssheet, j, 5);
							NDIRAWIN.Address = xl.getCellData(xlfile, tssheet, j, 6);
							NDIRAWIN.City = xl.getCellData(xlfile, tssheet, j, 7);
							NDIRAWIN.State = xl.getCellData(xlfile, tssheet, j, 8);
							NDIRAWIN.Zip = xl.getCellData(xlfile, tssheet, j, 9);
							NDIRAWIN.accname = xl.getCellData(xlfile, tssheet, j, 10);
							NDIRAWIN.AccountNumber = xl.getCellData(xlfile, tssheet, j, 11);
							NDIRAWIN.RoutingNumber = xl.getCellData(xlfile, tssheet, j, 12);
							res=NDIRAWIN.NDIRAWire_Instructions_NegativeTest();
							out.Logout();
							break;	
							
						case "WIREINSTRUCTIONSNEGATIVE4":
							NDIRAWIN.BankName = xl.getCellData(xlfile, tssheet, j, 5);
							NDIRAWIN.Address = xl.getCellData(xlfile, tssheet, j, 6);
							NDIRAWIN.City = xl.getCellData(xlfile, tssheet, j, 7);
							NDIRAWIN.State = xl.getCellData(xlfile, tssheet, j, 8);
							NDIRAWIN.Zip = xl.getCellData(xlfile, tssheet, j, 9);
							NDIRAWIN.accname = xl.getCellData(xlfile, tssheet, j, 10);
							NDIRAWIN.AccountNumber = xl.getCellData(xlfile, tssheet, j, 11);
							NDIRAWIN.RoutingNumber = xl.getCellData(xlfile, tssheet, j, 12);
							res=NDIRAWIN.NDIRAWire_Instructions_NegativeTest();
							out.Logout();
							break;	
							
						case "WIREINSTRUCTIONSNEGATIVE5":
							NDIRAWIN.BankName = xl.getCellData(xlfile, tssheet, j, 5);
							NDIRAWIN.Address = xl.getCellData(xlfile, tssheet, j, 6);
							NDIRAWIN.City = xl.getCellData(xlfile, tssheet, j, 7);
							NDIRAWIN.State = xl.getCellData(xlfile, tssheet, j, 8);
							NDIRAWIN.Zip = xl.getCellData(xlfile, tssheet, j, 9);
							NDIRAWIN.accname = xl.getCellData(xlfile, tssheet, j, 10);
							NDIRAWIN.AccountNumber = xl.getCellData(xlfile, tssheet, j, 11);
							NDIRAWIN.RoutingNumber = xl.getCellData(xlfile, tssheet, j, 12);
							res=NDIRAWIN.NDIRAWire_Instructions_NegativeTest();
							out.Logout();
							break;	
							
						case "WIREINSTRUCTIONSNEGATIVE6":
							NDIRAWIN.BankName = xl.getCellData(xlfile, tssheet, j, 5);
							NDIRAWIN.Address = xl.getCellData(xlfile, tssheet, j, 6);
							NDIRAWIN.City = xl.getCellData(xlfile, tssheet, j, 7);
							NDIRAWIN.State = xl.getCellData(xlfile, tssheet, j, 8);
							NDIRAWIN.Zip = xl.getCellData(xlfile, tssheet, j, 9);
							NDIRAWIN.accname = xl.getCellData(xlfile, tssheet, j, 10);
							NDIRAWIN.AccountNumber = xl.getCellData(xlfile, tssheet, j, 11);
							NDIRAWIN.RoutingNumber = xl.getCellData(xlfile, tssheet, j, 12);
							res=NDIRAWIN.NDIRAWire_Instructions_NegativeTest();
							out.Logout();
							break;
							
						case "ADDNDTCOUSERSNEGATIVE":
							nun.fname=xl.getCellData(xlfile, tssheet, j, 5);
							nun.lname=xl.getCellData(xlfile, tssheet, j, 6);
							nun.email=xl.getCellData(xlfile, tssheet, j, 7);
							res = nun.NDTCOUsersNegativeTest();
							out.Logout();
							break;
							
						case "ADDNDTCOUSERSNEGATIVE1":
							nun.fname=xl.getCellData(xlfile, tssheet, j, 5);
							nun.lname=xl.getCellData(xlfile, tssheet, j, 6);
							nun.email=xl.getCellData(xlfile, tssheet, j, 7);
							res = nun.NDTCOUsersNegativeTest();
							out.Logout();
							break;
							
						case "ADDNDTCOUSERSNEGATIVE2":
							nun.fname=xl.getCellData(xlfile, tssheet, j, 5);
							nun.lname=xl.getCellData(xlfile, tssheet, j, 6);
							nun.email=xl.getCellData(xlfile, tssheet, j, 7);
							res = nun.NDTCOUsersNegativeTest();
							out.Logout();
							break;
							
						case "ADDNDTCOUSERSNEGATIVE3":
							nun.fname=xl.getCellData(xlfile, tssheet, j, 5);
							nun.lname=xl.getCellData(xlfile, tssheet, j, 6);
							nun.email=xl.getCellData(xlfile, tssheet, j, 7);
							res = nun.NDTCOUsersNegativeTest();
							out.Logout();
							break;
							
						case "ADDNDTCOUSERSNEGATIVE4":
							nun.fname=xl.getCellData(xlfile, tssheet, j, 5);
							nun.lname=xl.getCellData(xlfile, tssheet, j, 6);
							nun.email=xl.getCellData(xlfile, tssheet, j, 7);
							res = nun.NDTCOUsersNegativeTest();
							out.Logout();
							break;
									
						case "ADDNDTCOUSERSNEGATIVE5":
							nun.fname=xl.getCellData(xlfile, tssheet, j, 5);
							nun.lname=xl.getCellData(xlfile, tssheet, j, 6);
							nun.email=xl.getCellData(xlfile, tssheet, j, 7);
							res = nun.NDTCOUsersNegativeTest();
							out.Logout();
							break;
							
						case "ADDNDTCOUSERSNEGATIVE6":
							nun.fname=xl.getCellData(xlfile, tssheet, j, 5);
							nun.lname=xl.getCellData(xlfile, tssheet, j, 6);
							nun.email=xl.getCellData(xlfile, tssheet, j, 7);
							res = nun.NDTCOUsersNegativeTest();
							out.Logout();
							break;
							
						case "ADDNDTCOUSERSNEGATIVE7":
							nun.fname=xl.getCellData(xlfile, tssheet, j, 5);
							nun.lname=xl.getCellData(xlfile, tssheet, j, 6);
							nun.email=xl.getCellData(xlfile, tssheet, j, 7);
							res = nun.NDTCOUsersNegativeTest();
							out.Logout();
							break;
							
							
						case "SPONSORWIREINSTRUCTIONS":
							swi.BankName = xl.getCellData(xlfile, tssheet, j, 5);
							swi.Address = xl.getCellData(xlfile, tssheet, j, 6);
							swi.City = xl.getCellData(xlfile, tssheet, j, 7);
							swi.State = xl.getCellData(xlfile, tssheet, j, 8);
							swi.Zip = xl.getCellData(xlfile, tssheet, j, 9);
							swi.accname = xl.getCellData(xlfile, tssheet, j, 10);
							swi.AccountNumber = xl.getCellData(xlfile, tssheet, j, 11);
							swi.RoutingNumber = xl.getCellData(xlfile, tssheet, j, 12);
							res=swi.SponsorWireInstructionsTest();
							slog.Logout();
							break;	
							
						case "ALLOCATIONSVALIDATION":
							av.Days = xl.getCellData(xlfile, tssheet, j, 5);
							av.Min = xl.getCellData(xlfile, tssheet, j, 6);
							av.Max = xl.getCellData(xlfile, tssheet, j, 7);
							res = av.SponsorAllocationsValidationTest();
							out.Logout();
							break;
							
						case "ALLOCATIONSVALIDATION1":
							av.Days = xl.getCellData(xlfile, tssheet, j, 5);
							av.Min = xl.getCellData(xlfile, tssheet, j, 6);
							av.Max = xl.getCellData(xlfile, tssheet, j, 7);
							res = av.SponsorAllocationsValidationTest();
							out.Logout();
							break;
							
						case "INVESTMENTCOMPONENTSSAVE":							
							res=ics.investmentComponentsSave();
							out.Logout();
							break;
							
						case "INVESTMENTCOMPONENTSCLIENTRELATIONSHIP":													
							iccr.fname = xl.getCellData(xlfile, tssheet, j, 5);
							iccr.lname = xl.getCellData(xlfile, tssheet, j, 6);
							iccr.email = xl.getCellData(xlfile, tssheet, j, 7);
							iccr.pass = xl.getCellData(xlfile, tssheet, j, 8);
							iccr.cpass = xl.getCellData(xlfile, tssheet, j, 9);
							iccr.DOB = xl.getCellData(xlfile, tssheet, j, 10);
							iccr.SSN = xl.getCellData(xlfile, tssheet, j, 11);
							iccr.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);
							iccr.phone = xl.getCellData(xlfile, tssheet, j, 13);
							iccr.address1 = xl.getCellData(xlfile, tssheet, j, 14);
							iccr.address2 = xl.getCellData(xlfile, tssheet, j, 15);
							iccr.city = xl.getCellData(xlfile, tssheet, j, 16);
							iccr.state = xl.getCellData(xlfile, tssheet, j, 17);
							iccr.zipcode = xl.getCellData(xlfile, tssheet, j, 18);
							iccr.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);
							res = iccr.investmentComponentsClientRelationship();
							d.get(url);
							out.Logout();
							break;
							
						case "INVESTMENTCOMPONENTSSAVEALLOCATION":						
							icsa.fname = xl.getCellData(xlfile, tssheet, j, 5);
							icsa.lname = xl.getCellData(xlfile, tssheet, j, 6);
							icsa.email = xl.getCellData(xlfile, tssheet, j, 7);
							icsa.pass = xl.getCellData(xlfile, tssheet, j, 8);
							icsa.cpass = xl.getCellData(xlfile, tssheet, j, 9);
							icsa.DOB = xl.getCellData(xlfile, tssheet, j, 10);
							icsa.SSN = xl.getCellData(xlfile, tssheet, j, 11);
							icsa.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);
							icsa.phone = xl.getCellData(xlfile, tssheet, j, 13);
							icsa.address1 = xl.getCellData(xlfile, tssheet, j, 14);
							icsa.address2 = xl.getCellData(xlfile, tssheet, j, 15);
							icsa.city = xl.getCellData(xlfile, tssheet, j, 16);
							icsa.state = xl.getCellData(xlfile, tssheet, j, 17);
							icsa.zipcode = xl.getCellData(xlfile, tssheet, j, 18);
							icsa.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);
							res=icsa.investmentComponentsSetAllocation();
							out.Logout();
							
							break;
							
						case "INVESTMENTCOMPONENTSALLAVAILABLEFUNDS":						
							icaf.fname = xl.getCellData(xlfile, tssheet, j, 5);
							icaf.lname = xl.getCellData(xlfile, tssheet, j, 6);
							icaf.email = xl.getCellData(xlfile, tssheet, j, 7);
							icaf.pass = xl.getCellData(xlfile, tssheet, j, 8);
							icaf.cpass = xl.getCellData(xlfile, tssheet, j, 9);
							icaf.DOB = xl.getCellData(xlfile, tssheet, j, 10);
							icaf.SSN = xl.getCellData(xlfile, tssheet, j, 11);
							icaf.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);
							icaf.phone = xl.getCellData(xlfile, tssheet, j, 13);
							icaf.address1 = xl.getCellData(xlfile, tssheet, j, 14);
							icaf.address2 = xl.getCellData(xlfile, tssheet, j, 15);
							icaf.city = xl.getCellData(xlfile, tssheet, j, 16);
							icaf.state = xl.getCellData(xlfile, tssheet, j, 17);
							icaf.zipcode = xl.getCellData(xlfile, tssheet, j, 18);
							icaf.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);						
							res=icaf.investmentComponentsAllAvailableFunds();
							out.Logout();
							break;
							
						case "INVESTMENTCOMPONENTSDEPOSITORYSTORAGEOPTIONS":							
							icso.fname = xl.getCellData(xlfile, tssheet, j, 5);
							icso.lname = xl.getCellData(xlfile, tssheet, j, 6);
							icso.email = xl.getCellData(xlfile, tssheet, j, 7);
							icso.pass = xl.getCellData(xlfile, tssheet, j, 8);
							icso.cpass = xl.getCellData(xlfile, tssheet, j, 9);
							icso.DOB = xl.getCellData(xlfile, tssheet, j, 10);
							icso.SSN = xl.getCellData(xlfile, tssheet, j, 11);
							icso.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);
							icso.phone = xl.getCellData(xlfile, tssheet, j, 13);
							icso.address1 = xl.getCellData(xlfile, tssheet, j, 14);
							icso.address2 = xl.getCellData(xlfile, tssheet, j, 15);
							icso.city = xl.getCellData(xlfile, tssheet, j, 16);
							icso.state = xl.getCellData(xlfile, tssheet, j, 17);
							icso.zipcode = xl.getCellData(xlfile, tssheet, j, 18);
							icso.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);								
							res=icso.investComponentsDepositoryStorageOptions();
							out.Logout();
							break;
							
							
						case "INVESTMENTCOMPONENTSENABLETRANSACTIONFEE":							
							ictf.fname = xl.getCellData(xlfile, tssheet, j, 5);
							ictf.lname = xl.getCellData(xlfile, tssheet, j, 6);
							ictf.email = xl.getCellData(xlfile, tssheet, j, 7);
							ictf.pass = xl.getCellData(xlfile, tssheet, j, 8);
							ictf.cpass = xl.getCellData(xlfile, tssheet, j, 9);
							ictf.DOB = xl.getCellData(xlfile, tssheet, j, 10);
							ictf.SSN = xl.getCellData(xlfile, tssheet, j, 11);
							ictf.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);
							ictf.phone = xl.getCellData(xlfile, tssheet, j, 13);
							ictf.address1 = xl.getCellData(xlfile, tssheet, j, 14);
							ictf.address2 = xl.getCellData(xlfile, tssheet, j, 15);
							ictf.city = xl.getCellData(xlfile, tssheet, j, 16);
							ictf.state = xl.getCellData(xlfile, tssheet, j, 17);
							ictf.zipcode = xl.getCellData(xlfile, tssheet, j, 18);
							ictf.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);	
							res=ictf.investComponentsEnableTransactionFee();
							out.Logout();
							break;
							
						case "INVESTMENTCOMPONENTSCOURTESY":							
                	   		iccc.fname = xl.getCellData(xlfile, tssheet, j, 5);
                	   		iccc.lname = xl.getCellData(xlfile, tssheet, j, 6);
                	   		iccc.email = xl.getCellData(xlfile, tssheet, j, 7);
                	   		iccc.pass = xl.getCellData(xlfile, tssheet, j, 8);
                	   		iccc.cpass = xl.getCellData(xlfile, tssheet, j, 9);
                	   		iccc.DOB = xl.getCellData(xlfile, tssheet, j, 10);
                	   		iccc.SSN = xl.getCellData(xlfile, tssheet, j, 11);
                	   		iccc.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);
                	   		iccc.phone = xl.getCellData(xlfile, tssheet, j, 13);
                	   		iccc.address1 = xl.getCellData(xlfile, tssheet, j, 14);
                	   		iccc.address2 = xl.getCellData(xlfile, tssheet, j, 15);
                	   		iccc.city = xl.getCellData(xlfile, tssheet, j, 16);
                	   		iccc.state = xl.getCellData(xlfile, tssheet, j, 17);
                	   		iccc.zipcode = xl.getCellData(xlfile, tssheet, j, 18);
                	   		iccc.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);						
							res=iccc.investmentComponentsCourtesycall();
							out.Logout();
							break;
							
                   case "INVESTMENTTERMSANDCONDITIONS":						
                	   	ictc.fname = xl.getCellData(xlfile, tssheet, j, 5);
                	   	ictc.lname = xl.getCellData(xlfile, tssheet, j, 6);
                	   	ictc.email = xl.getCellData(xlfile, tssheet, j, 7);
                	   	ictc.pass = xl.getCellData(xlfile, tssheet, j, 8);
                	   	ictc.cpass = xl.getCellData(xlfile, tssheet, j, 9);
                	   	ictc.DOB = xl.getCellData(xlfile, tssheet, j, 10);
                	   	ictc.SSN = xl.getCellData(xlfile, tssheet, j, 11);
                	   	ictc.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);
                	   	ictc.phone = xl.getCellData(xlfile, tssheet, j, 13);
                	   	ictc.address1 = xl.getCellData(xlfile, tssheet, j, 14);
                	   	ictc.address2 = xl.getCellData(xlfile, tssheet, j, 15);
                	   	ictc.city = xl.getCellData(xlfile, tssheet, j, 16);
                	   	ictc.state = xl.getCellData(xlfile, tssheet, j, 17);
                	   	ictc.zipcode = xl.getCellData(xlfile, tssheet, j, 18);
                	   	ictc.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);						
						res=ictc.investmentComponentsInvestmentTermsandConditions();
						out.Logout();
						break;
						
						
                   case "CLIENTRELATIONSHIPBASEDONSPONSORRELATIONSHIPADVISOR":	                	   	          	  
					    res=cfsr.clientRelationshipforSponsorRelationshipAdvisor();
						out.Logout();
						break;
					
                   case "CLIENTRELATIONSHIPBASEDONSPONSORRELATIONSHIPREFERRER":	               	   
					    res=cfsrr.clientRelationshipforSponsorrelationshipReferrer();
						out.Logout();
						break;
						
                   case "CLIENTRELATIONSHIPBASEDONSPONSORRELATIONSHIPPROVIDER":	                	   
					    res=csrp.clientRelationshipforSponsorRelationshipprovider();
						out.Logout();
						break;
                   case "INVESTMENTCOMPONENTSDEFAULTFORADMIN":                	   
					    res=icda.investmentComponentsdefaultforAdmin();
						out.Logout();
						break;
						
                   case "INVESTMENTCOMPONENTSDEFAULTFORREFERRER":	                	   
					    res=icdr.investmentComponentsdefaultforReferrer();
						out.Logout();
						break;
						
                   case "INVESTMENTCOMPONENTSDEFAULTFORPROVIDER":
					    res=icdp.investmentComponentsdefaultforprovider();
						out.Logout();
						break;
						
                   case "CLIENTSIGNUPTHIRDSTEPHEADINGFORADVISOR":	                	  						
                	   csha.fname = xl.getCellData(xlfile, tssheet, j, 5);
                	   csha.lname = xl.getCellData(xlfile, tssheet, j, 6);
                	   csha.email = xl.getCellData(xlfile, tssheet, j, 7);
                	   csha.pass = xl.getCellData(xlfile, tssheet, j, 8);
                	   csha.cpass = xl.getCellData(xlfile, tssheet, j, 9);
                	   csha.DOB = xl.getCellData(xlfile, tssheet, j, 10);
                	   csha.SSN = xl.getCellData(xlfile, tssheet, j, 11);
                	   csha.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);
                	   csha.phone = xl.getCellData(xlfile, tssheet, j, 13);
                	   csha.address1 = xl.getCellData(xlfile, tssheet, j, 14);
                	   csha.address2 = xl.getCellData(xlfile, tssheet, j, 15);
                	   csha.city = xl.getCellData(xlfile, tssheet, j, 16);
                	   csha.state = xl.getCellData(xlfile, tssheet, j, 17);
                	   csha.zipcode = xl.getCellData(xlfile, tssheet, j, 18);
                	   csha.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);						
                	   res=csha.clientSignupStep3headingforAdvisor();
                	   out.Logout();
                	   break;
						
						
                   case "CLIENTSIGNUPTHIRDSTEPHEADINGFORREFERRER":					
                	   cshr.fname = xl.getCellData(xlfile, tssheet, j, 5);
                	   cshr.lname = xl.getCellData(xlfile, tssheet, j, 6);
                	   cshr.email = xl.getCellData(xlfile, tssheet, j, 7);
                	   cshr.pass = xl.getCellData(xlfile, tssheet, j, 8);
                	   cshr.cpass = xl.getCellData(xlfile, tssheet, j, 9);
                	   cshr.DOB = xl.getCellData(xlfile, tssheet, j, 10);
                	   cshr.SSN = xl.getCellData(xlfile, tssheet, j, 11);
                	   cshr.maritalstatus = xl.getCellData(xlfile, tssheet, j, 13);
                	   cshr.phone = xl.getCellData(xlfile, tssheet, j, 14);
                	   cshr.address1 = xl.getCellData(xlfile, tssheet, j, 15);
                	   cshr.address2 = xl.getCellData(xlfile, tssheet, j, 16);
                	   cshr.city = xl.getCellData(xlfile, tssheet, j, 16);
                	   cshr.state = xl.getCellData(xlfile, tssheet, j, 17);
                	   cshr.zipcode = xl.getCellData(xlfile, tssheet, j, 18);
                	   cshr.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);						
						res=cshr.clientSignupStep3headingforReferrer();
						out.Logout();
						break;
						
                   case "ADDINGDEPOSITORIES":	                	  
					    res=ad.addingDepositories();
						out.Logout();
						break;
							
							
                   case "PREPAYMENT":		                	  					
                	   pp.fname = xl.getCellData(xlfile, tssheet, j, 5);
                	   pp.lname = xl.getCellData(xlfile, tssheet, j, 6);
                	   pp.email = xl.getCellData(xlfile, tssheet, j, 7);
                	   pp.pass = xl.getCellData(xlfile, tssheet, j, 8);
                	   pp.cpass = xl.getCellData(xlfile, tssheet, j, 9);
                	   pp.DOB = xl.getCellData(xlfile, tssheet, j, 10);
                	   pp.SSN = xl.getCellData(xlfile, tssheet, j, 11);
                	   pp.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);
                	   pp.phone = xl.getCellData(xlfile, tssheet, j, 13);
                	   pp.address1 = xl.getCellData(xlfile, tssheet, j, 14);
                	   pp.address2 = xl.getCellData(xlfile, tssheet, j, 15);
                	   pp.city = xl.getCellData(xlfile, tssheet, j, 16);
                	   pp.state = xl.getCellData(xlfile, tssheet, j, 17);
                	   pp.zipcode = xl.getCellData(xlfile, tssheet, j, 18);
                	   pp.Amount = xl.getCellData(xlfile, tssheet, j, 19);
                	   pp.typeofaccount = xl.getCellData(xlfile, tssheet, j, 20);						
                	   res=pp.PrepaymentTest();
                	   out.Logout();
                	   break;
					
                   case "ACCOUNTOPENINGPAYMENT":	                	  					
                	   client.fname = xl.getCellData(xlfile, tssheet, j, 5);
                	   client.lname = xl.getCellData(xlfile, tssheet, j, 6);
                	   client.email = xl.getCellData(xlfile, tssheet, j, 7);
                	   client.pass = xl.getCellData(xlfile, tssheet, j, 8);
                	   client.cpass = xl.getCellData(xlfile, tssheet, j, 9);
                	   client.DOB = xl.getCellData(xlfile, tssheet, j, 10);
                	   client.SSN = xl.getCellData(xlfile, tssheet, j, 11);
                	   client.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);
                	   client.phone = xl.getCellData(xlfile, tssheet, j, 13);
                	   client.address1 = xl.getCellData(xlfile, tssheet, j, 14);
                	   client.address2 = xl.getCellData(xlfile, tssheet, j, 15);
                	   client.city = xl.getCellData(xlfile, tssheet, j, 16);
                	   client.state = xl.getCellData(xlfile, tssheet, j, 17);
                	   client.zipcode = xl.getCellData(xlfile, tssheet, j, 18);
                	   client.Amount = xl.getCellData(xlfile, tssheet, j, 19);
                	   client.typeofaccount = xl.getCellData(xlfile, tssheet, j, 20);                	   
                	   res=client.AccountOpeningPaymentClientTest();
                	   out.Logout();
                	   break;                	   
                	   
                   case "SITEDATAFEEUPDATE":
                       feeupdate.aname=xl.getCellData(xlfile, tssheet, j, 5);
                       feeupdate.adisplayname=xl.getCellData(xlfile, tssheet, j, 6);
                       feeupdate.aoraclecode=xl.getCellData(xlfile, tssheet, j, 7);
                       feeupdate.afeeschedulename=xl.getCellData(xlfile, tssheet, j, 8);
                       feeupdate.aDisplaytext=xl.getCellData(xlfile, tssheet, j, 9);
                       res=feeupdate.Sitedata_FeeSchedule_UpdateTest(feeupdate.aname, feeupdate.adisplayname, feeupdate.afeeschedulename, feeupdate.aoraclecode, feeupdate.aDisplaytext);
                       out.Logout();
                       break;
                	   
                   case "DEPOSITORYINFORMATION":	                  	 
                	   dinf.asponsorname= finalsponsor;						
                	   dinf.fname = xl.getCellData(xlfile, tssheet, j, 5);
                	   dinf.lname = xl.getCellData(xlfile, tssheet, j, 6);
                	   dinf.email = xl.getCellData(xlfile, tssheet, j, 7);
                	   dinf.pass = xl.getCellData(xlfile, tssheet, j, 8);
                	   dinf.cpass = xl.getCellData(xlfile, tssheet, j, 9);
                	   dinf.DOB = xl.getCellData(xlfile, tssheet, j, 10);
                	   dinf.SSN = xl.getCellData(xlfile, tssheet, j, 11);
                	   dinf.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);
                	   dinf.phone = xl.getCellData(xlfile, tssheet, j, 13);
                	   dinf.address1 = xl.getCellData(xlfile, tssheet, j, 14);
                	   dinf.address2 = xl.getCellData(xlfile, tssheet, j, 15);
                	   dinf.city = xl.getCellData(xlfile, tssheet, j, 16);
                	   dinf.state = xl.getCellData(xlfile, tssheet, j, 17);
                	   dinf.zipcode = xl.getCellData(xlfile, tssheet, j, 18);
                	   dinf.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);						
                	   res=dinf.depositoryInformationInStep3Validation(dinf.asponsorname);						
                	   out.Logout();
                	   break;
						
                   case "ADDFEESCHEDULE":
                	   afs.afeeshedulename=xl.getCellData(xlfile,tssheet, j, 5);
                	   res=afs.addFeeScheduleinFees(afs.afeeshedulename);
                	   out.Logout();
                	   break;
						
                   case "FEENAMEDISPLAYINGINOPENACCOUNT":                  	 
                	   fso.asponsorname= finalsponsor;				
                	   fso.fname = xl.getCellData(xlfile, tssheet, j, 5);
                	   fso.lname = xl.getCellData(xlfile, tssheet, j, 6);
                	   fso.email = xl.getCellData(xlfile, tssheet, j, 7);
                	   fso.pass = xl.getCellData(xlfile, tssheet, j, 8);
                	   fso.cpass = xl.getCellData(xlfile, tssheet, j, 9);
                	   fso.DOB = xl.getCellData(xlfile, tssheet, j, 10);
                	   fso.SSN = xl.getCellData(xlfile, tssheet, j, 11);
                	   fso.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);
                	   fso.phone = xl.getCellData(xlfile, tssheet, j, 13);
                	   fso.address1 = xl.getCellData(xlfile, tssheet, j, 14);
                	   fso.address2 = xl.getCellData(xlfile, tssheet, j, 15);
                	   fso.city = xl.getCellData(xlfile, tssheet, j, 16);
                	   fso.state = xl.getCellData(xlfile, tssheet, j, 17);
                	   fso.zipcode = xl.getCellData(xlfile, tssheet, j, 18);
                	   fso.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);						
						res=fso.feeScheduleNameDisplayinginOpenAccount(fso.asponsorname);						
						out.Logout();
						break;
						
                   case "ADDEDFEEVALIDATION":
                	   fv.asponsorname=finalsponsor;
                	   fv.name=xl.getCellData(xlfile, tssheet, j, 5);
                	   fv.displayname=xl.getCellData(xlfile, tssheet, j, 6);							
                	   fv.oraclecode=xl.getCellData(xlfile, tssheet, j, 7);
                	   fv.feeschedulename=xl.getCellData(xlfile, tssheet, j, 8);
                	   fv.Displaytext=xl.getCellData(xlfile, tssheet, j, 9);
                	   res=fv.FeesValidationTest(fv.asponsorname);
                	   out.Logout();
                	   break;
							
                   case "SITEDATAADDFEEDETAILS":
                	   afd.displayname=xl.getCellData(xlfile, tssheet, j, 5);
                	   afd.Feedetailsname=xl.getCellData(xlfile, tssheet, j, 6);
                	   afd.Feetype=xl.getCellData(xlfile, tssheet, j, 7);
                	   afd.amount=xl.getCellData(xlfile, tssheet, j, 8);
                	   afd.Description=xl.getCellData(xlfile, tssheet, j, 9);
                	   res=afd.Sitedata_Add_Fee_detailsTest();
                	   out.Logout();
                	   break;

						default:
							break;
					}

						String tsres = null;

						if (res)
						{
							tsres = "Pass";
							xl.setCellData(xlfile, tssheet, j, 3, tsres);
							xl.fillGreenColor(xlfile, tssheet, j, 3);
							tcres = tsres;
							xl.setCellData(xlfile, tcsheet, i, 3, tcres);
							xl.fillGreenColor(xlfile, tcsheet, i, 3);
						} 
						else 
						{
							tsres = "Fail";
							xl.setCellData(xlfile, tssheet, j, 3, tsres);
							xl.fillRedColor(xlfile, tssheet, j, 3);
							tcres = tsres;
							xl.setCellData(xlfile, tcsheet, i, 3, tcres);
							xl.fillRedColor(xlfile, tcsheet, i, 3);
						}
					}
				}
			} 
			else 
			{
				xl.setCellData(xlfile, tcsheet, i, 3, "Not Executed");
				xl.fillOrangeColor(xlfile, tcsheet, i, 3);
				
			}
		}
	}

}
