package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class SponsorAddInvestiment extends NDiraConstantsV2
{
	public static String name,Investment,text;
	public static boolean SponsorAddInvestment() throws IOException
	{
		try
		{		
		  	Sponsorlogout no=new Sponsorlogout();
		  	no.mLogin();
		  	
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  	d.findElement(By.linkText("Add Investment")).click();
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/div/form/div[1]/div[1]/div/input")).sendKeys(name);
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  	Select Inv=new Select(d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/div/form/div[1]/div[2]/div/select")));
		  	Inv.selectByVisibleText(Investment);
		  	
		  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/div/form/div[1]/div[3]/div/textarea")).sendKeys(text);
		  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/div/form/ul/li[2]/button")).click();
		  	Sleeper.sleepTightInSeconds(2);
		  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/div/form/div[2]/div/div[1]")).click();
		  	Sleeper.sleepTightInSeconds(5);	  	
		  	String windows_component = NdiraUtilsV2.getProperty("file.windows.component");
		  	Runtime.getRuntime().exec(windows_component);
		  	
		  	String expmsg,acmsg;
		  	expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/div/form/p[2]")).getText();
		  	acmsg="Investment saved.";
		  	if (expmsg.equalsIgnoreCase(acmsg))
		  	{
		  		return true;	
			} 
		  	else
		  	{
		  		return false;
			}
		}
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	  	
	}
}
