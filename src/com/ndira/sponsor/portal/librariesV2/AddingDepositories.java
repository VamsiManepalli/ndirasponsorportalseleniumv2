
package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;



public class AddingDepositories extends NDiraConstantsV2
{
	public static boolean addingDepositories() throws IOException
	{
		try 
		{
			XLUtilsV2 xl=new XLUtilsV2();
			String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			Logout no=new Logout();
			no.mLogin();
			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
			d.findElement(By.linkText("Sponsors")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText(sponsor)).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			JavascriptExecutor js = (JavascriptExecutor)d;
	        js.executeScript("window.scrollBy(0,+1800)", "");
	        
	        d.findElement(By.linkText("Depositories")).click();
	        d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/div[2]/div[1]/div[4]/h3/span/i[1]")).click();
	        d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/div[2]/div[1]/div[4]/div/div/div/ul/li[2]/div/label")).click();
		    String depname=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/div[2]/div[1]/div[4]/div/div/div/ul/li[2]/div/label")).getText();
		    System.out.println(depname);
		    List<WebElement> li=d.findElements(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/div[2]/div[2]/div/div/label"));
		    System.out.println(li.size());
		    boolean flag=true;
		    for (int i = 1; i <li.size(); i++) 
			{
				String names=li.get(i).getText();
				System.out.println(names);
				if (names.contains(depname))
				{	
					return flag=true;			
				} 
				else 
				{
					return flag=false;
				}	
			}
			if (flag=true) 
			{
				return true;					
			} 
			else
			{
				return false;
			}	
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}




