package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class UserSignUpwithSponser extends NDiraConstantsV2
{
	public static String fname,lname,email,pass,cpass,DOB,SSN,maritalstatus,phone,address1,address2,city,state,zipcode,typeofaccount,investiment,phonen,cardno,cardExpiryDate,cardName,cardCVCNumber;
	public static boolean ClientSignUp() throws IOException
	{
		try
		{
			XLUtilsV2 xl=new XLUtilsV2();
			String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			Sponsorlogout no=new Sponsorlogout();
		  	no.mLogin();
		  	Sleeper.sleepTightInSeconds(2);
		  	d.get("http://ndira-my-direction-portal-orn-upgrade.qbjenjfbn7.us-east-1.elasticbeanstalk.com/authentication/open-account?sponsor=ND100175");
		  Sleeper.sleepTightInSeconds(2);
		 
		  d.findElement(By.id("firstName")).sendKeys(fname);
		  
		  d.findElement(By.id("lastName")).sendKeys(lname);
		  
		  d.findElement(By.id("emailAddress")).sendKeys(email);
		
		  d.findElement(By.id("password")).sendKeys(pass);
		  
		  d.findElement(By.id("confirmPassword")).sendKeys(cpass);
		  d.findElement(By.id("nextButton")).click();
		  Sleeper.sleepTightInSeconds(2);
		 
		  d.findElement(By.id("dateOfBirth")).sendKeys(DOB);
		  
		  d.findElement(By.id("SSN")).sendKeys(SSN);
		
		  Select ms=new Select(d.findElement(By.id("maritalStatus")));
		  ms.selectByVisibleText(maritalstatus);
		
		  d.findElement(By.id("phone")).sendKeys(phone);
		  
		  d.findElement(By.id("street")).sendKeys(address1);
		  
		  d.findElement(By.id("aptSte")).sendKeys(address2);
		  
		  d.findElement(By.id("city")).sendKeys(city);
		 
		  Select st=new Select(d.findElement(By.id("state")));
		  st.selectByVisibleText(state);
		  
		  d.findElement(By.id("zip")).sendKeys(zipcode);
		  Sleeper.sleepTightInSeconds(2);
		  
		  Select toa=new Select(d.findElement(By.id("accountType")));
		  toa.selectByVisibleText(typeofaccount);
		  Sleeper.sleepTightInSeconds(2);
		  d.findElement(By.xpath("html/body/div[3]/form/div[6]/div/div[2]/div/div[1]/div/table/tbody/tr[1]/td/h5/label/span")).click();
		  Sleeper.sleepTightInSeconds(2);
		  d.findElement(By.xpath("html/body/div[3]/form/div[7]/div[2]/div[2]/label/span")).click();
		  d.findElement(By.id("nextButton")).click();
		  Sleeper.sleepTightInSeconds(3);
		  
		  d.findElement(By.id("investmentAmount")).sendKeys(investiment);
		  Sleeper.sleepTightInSeconds(3);
		  d.findElement(By.xpath("html/body/div[3]/form/div[1]/div[4]/div[2]/label/span")).click();
		  Sleeper.sleepTightInSeconds(3);
		  d.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label/span")).click();
		  Sleeper.sleepTightInSeconds(2);
		 
		  d.findElement(By.id("contactMePhone")).sendKeys(phonen);
		  Sleeper.sleepTightInSeconds(3);
		  d.findElement(By.xpath("html/body/div[3]/form/div[3]/div[2]/div[2]/label[1]/span")).click();
		  Sleeper.sleepTightInSeconds(2);
		  d.findElement(By.xpath("html/body/div[3]/form/div[4]/div[2]/div[2]/label/span")).click();
		  Sleeper.sleepTightInSeconds(2);
		  d.findElement(By.id("nextButton")).click();
		  
		  d.findElement(By.id("cardNumber")).sendKeys(cardno);
		 
		  d.findElement(By.id("cardExpiryDate")).sendKeys(cardExpiryDate);
		  
		  d.findElement(By.id("cardName")).sendKeys(cardName);
		  
		  d.findElement(By.id("cardCVCNumber")).sendKeys(cardCVCNumber);
		  Sleeper.sleepTightInSeconds(4);
		  d.findElement(By.xpath("html/body/div[3]/form/div[5]/div[2]/div[2]/label[1]/span")).click();
		  Sleeper.sleepTightInSeconds(4);
		  d.findElement(By.xpath("html/body/div[3]/form/div[7]/div[2]/div[2]/label/span[1]")).click();
		  Sleeper.sleepTightInSeconds(2);
		  d.findElement(By.id("nextButton")).click();
		  
		  String AccNo;
		  AccNo=d.findElement(By.xpath("html/body/div[4]/div/div/div[2]/div/p[2]")).getText();
		  xl.setCellData(xlfile, tssheet, 21, 26, AccNo);
		  
		  String expurl,acturl;
		  expurl="http://mydirection.qa.newdirectionira.com/new_account/message";
		  acturl="http://mydirection.qa.newdirectionira.com/communications-error";
		  
		  if (expurl.contains(expurl))
		  {
			return true;
		  } 
		  else 
		  {
			  return false;
		  }
		
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	  	  
	}
}
