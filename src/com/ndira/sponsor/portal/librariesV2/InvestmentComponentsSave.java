package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class InvestmentComponentsSave extends NDiraConstantsV2
{	
	public static boolean investmentComponentsSave() throws IOException
	{
		try 
		{XLUtilsV2 xl=new XLUtilsV2();
		String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
		String tcsheet="TestCases";
		String tssheet="TestSteps";
		Logout no=new Logout();
		no.mLogin();
		
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
		d.findElement(By.linkText("Sponsors")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.linkText(sponsor)).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.linkText("Investment Components")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/button")).click();
		String exptext,actext;
		exptext="updated";
		actext=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[1]/div/div/div/span")).getText();
		System.out.println(actext);
		if (actext.contains(exptext))
		{
			return true;			
		} 
		else 
		{
			return false;
		}		
	} 
	catch (Exception e) 
	{
		System.out.println(e);
		return false;
	}
}
}
