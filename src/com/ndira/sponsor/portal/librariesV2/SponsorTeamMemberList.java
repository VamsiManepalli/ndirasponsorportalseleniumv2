package com.ndira.sponsor.portal.librariesV2;

import java.util.ArrayList;
import java.util.List;

public class SponsorTeamMemberList 
{
	public static List<String> getAddTeamMemberMessages() 
	{
		List<String> list = new ArrayList<>();
		//list.add("must provide email to create user");
		list.add("must provide first name, last name, and phone");
		list.add("Please enter a valid email.");
		return list;
	}
}
