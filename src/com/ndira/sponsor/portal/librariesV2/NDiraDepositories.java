package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class NDiraDepositories extends NDiraConstantsV2
{
	public static boolean Ndiradepositories() throws IOException
	{
		try 
		{		
			XLUtilsV2 xl=new XLUtilsV2();
			String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			Logout no=new Logout();
		  	no.mLogin();
		  	
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
		  	d.findElement(By.linkText("Sponsors")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
			String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
			d.findElement(By.linkText(sponsor)).click();
			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
			Sleeper.sleepTightInSeconds(2);
			Select investcat = new Select(d.findElement(By.id("primary-investment-category")));
			WebElement option = investcat.getFirstSelectedOption();
			String invvalue = option.getText();
			if (invvalue.equalsIgnoreCase("Metals"))
			{
				d.findElement(By.linkText("Depositories")).click();
				d.findElement(By.xpath(".//*[@id='sponsor-depositories']/div[2]/div[1]/div[2]/label/input")).click();
				Sleeper.sleepTightInSeconds(2);
				d.findElement(By.xpath(".//*[@id='sponsor-depositories']/div[2]/div[1]/div[1]/button")).click();
				String acmsg;
				Sleeper.sleepTightInSeconds(2);
				JavascriptExecutor jse = (JavascriptExecutor)d;
			    jse.executeScript("window.scrollBy(0,-250)", "");
			    Sleeper.sleepTightInSeconds(2);
				acmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div/div/span")).getText();
				Sleeper.sleepTightInSeconds(2);
				
				if (acmsg.equalsIgnoreCase("Depositories updated.")) 
				{
					return true;				
				} 
				else 
				{
					return false;
				}
			}
			else 
			{
				return true;
			}
	
		}
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
	}
}
