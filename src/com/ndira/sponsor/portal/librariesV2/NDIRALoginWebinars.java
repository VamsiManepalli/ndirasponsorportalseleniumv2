package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class NDIRALoginWebinars extends NDiraConstantsV2
{
	public static boolean NDIRALoginWebinars() throws IOException
	{
		try 
		{
			Logout no=new Logout();
		  	no.mLogin();
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  	d.findElement(By.linkText("Site Content")).click();
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.linkText("Webinars")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String exptext,actext;
			exptext=d.findElement(By.xpath(".//*[@id='admin-webinars-list']/div[1]/div/div/h1")).getText();
			actext="Webinars";
			if (exptext.equalsIgnoreCase(actext))
			{
				return true;			
			} 
			else 
			{
				return false;
			}	

		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
