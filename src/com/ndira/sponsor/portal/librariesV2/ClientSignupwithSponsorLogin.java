package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class ClientSignupwithSponsorLogin extends NDiraConstantsV2
{
	public static boolean ClientSignUp() throws IOException
	 {
		try 
		{
			XLUtilsV2 xl=new XLUtilsV2();
			String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			Sleeper.sleepTightInSeconds(2);
		  	Sponsorlogout no=new Sponsorlogout();
		  	no.mLogin();
		  	String userurl;
			userurl=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div[2]/div/p[2]/a")).getText();
			
			xl.setCellData(xlfile, tssheet, 22, 5,userurl);
			Sleeper.sleepTightInSeconds(3);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div[2]/div/p[2]/a")).click();
			Sleeper.sleepTightInSeconds(5);
			d.get(xl.getCellData(xlfile, tssheet, 22, 5));
			Sleeper.sleepTightInSeconds(3);
			  String fname=xl.getCellData(xlfile, tssheet, 21, 5);
			  d.findElement(By.id("firstName")).sendKeys(fname);
			  String lname=xl.getCellData(xlfile, tssheet, 21, 6);
			  d.findElement(By.id("lastName")).sendKeys(lname);
			  String email=xl.getCellData(xlfile, tssheet, 21, 7);
			  d.findElement(By.id("emailAddress")).sendKeys(email);
			  String pass=xl.getCellData(xlfile, tssheet, 21, 8);
			  d.findElement(By.id("password")).sendKeys(pass);
			  String cpass=xl.getCellData(xlfile, tssheet, 21, 9);
			  d.findElement(By.id("confirmPassword")).sendKeys(cpass);
			  d.findElement(By.id("nextButton")).click();
			  Sleeper.sleepTightInSeconds(2);
			  String DOB=xl.getCellData(xlfile, tssheet, 21, 10);
			  d.findElement(By.id("dateOfBirth")).sendKeys(DOB);
			  String SSN=xl.getCellData(xlfile, tssheet, 21, 11);
			  d.findElement(By.id("SSN")).sendKeys(SSN);
			  String maritalstatus= xl.getCellData(xlfile, tssheet, 21, 12);
			  Select ms=new Select(d.findElement(By.id("maritalStatus")));
			  ms.selectByVisibleText(maritalstatus);
			  String phone=xl.getCellData(xlfile, tssheet, 21, 13);
			  d.findElement(By.id("phone")).sendKeys(phone);
			  String address1=xl.getCellData(xlfile, tssheet, 21, 14);
			  d.findElement(By.id("street")).sendKeys(address1);
			  String address2=xl.getCellData(xlfile, tssheet, 21, 15);
			  d.findElement(By.id("aptSte")).sendKeys(address2);
			  String city=xl.getCellData(xlfile, tssheet, 21, 16);
			  d.findElement(By.id("city")).sendKeys(city);
			  String state=xl.getCellData(xlfile, tssheet, 21, 17);
			  Select st=new Select(d.findElement(By.id("state")));
			  st.selectByVisibleText(state);
			  String zipcode=xl.getCellData(xlfile, tssheet, 21, 18);
			  d.findElement(By.id("zip")).sendKeys(zipcode);
			  Sleeper.sleepTightInSeconds(2);
			  String typeofaccount= xl.getCellData(xlfile, tssheet, 21, 19);
			  Select toa=new Select(d.findElement(By.id("accountType")));
			  toa.selectByVisibleText(typeofaccount);
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.xpath("html/body/div[3]/form/div[6]/div/div[2]/div/div[1]/div/table/tbody/tr[1]/td/h5/label/span")).click();
			  d.findElement(By.xpath("html/body/div[3]/form/div[7]/div[2]/div[2]/label/span")).click();
			  d.findElement(By.id("nextButton")).click();
			  Sleeper.sleepTightInSeconds(3);
			  String investiment=xl.getCellData(xlfile, tssheet, 21, 20);
			  d.findElement(By.id("investmentAmount")).sendKeys(investiment);
			  d.findElement(By.xpath("html/body/div[3]/form/div/div[7]/div[2]/div[2]/label[1]/span")).click();
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.xpath("html/body/div[3]/form/div/div[5]/div[2]/label/span")).click();
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.xpath("html/body/div[3]/form/div/div[8]/div[2]/div[2]/label/span")).click();
			  Sleeper.sleepTightInSeconds(2);
			  String phonen=xl.getCellData(xlfile, tssheet, 21, 21);
			  d.findElement(By.id("contactMePhone")).sendKeys(phonen);
			  d.findElement(By.xpath("html/body/div[3]/form/div/div[9]/div[2]/div[2]/label/span")).click();
			  d.findElement(By.id("nextButton")).click();
			  
			  String expurl,acturl;
			  expurl=d.getCurrentUrl();
			  
			  if (expurl.contains(expurl))
			  {
				return true;
			  } 
			  else 
			  {
				  return false;
			  }	
			} 
			catch (Exception e) 
			{
			System.out.println(e);
			return false;
			} 
	 	}
}
