package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class SponsorMessageNegative extends NDiraConstantsV2
{
	public static boolean SponsorMessageNegative() throws IOException 
	{
		try
		{
			Sponsorlogout no=new Sponsorlogout();
		  	no.mLogin();
		  	d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  	d.findElement(By.linkText("Messages")).click();
		  	d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/div/div/div[1]/div/form/div[2]/div/button")).click();
		  	
		  	String expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/div/div/div[1]/div/p[2]")).getText();
		  	if (expmsg.contains("There was an error with your message. Please review and resubmit."))
		  	{
		  		return true;
			} 
		  	else 
		  	{
		  		return false;
			}	
		} 
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
	}
}
