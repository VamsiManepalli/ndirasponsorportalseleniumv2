
package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class ClientSignupStep3headingforReferrer extends NDiraConstantsV2
{
	public static String  fname,lname,email,pass,cpass,DOB,SSN,maritalstatus,phone,address1,address2,city,state,zipcode,typeofaccount;
	public static boolean clientSignupStep3headingforReferrer() throws IOException
	{
		try 
		{
			XLUtilsV2 xl=new XLUtilsV2();
			String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			Logout no=new Logout();
			no.mLogin();
			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
			d.findElement(By.linkText("Sponsors")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText(sponsor)).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			JavascriptExecutor js = (JavascriptExecutor)d;
	        js.executeScript("window.scrollBy(0,+1800)", "");
	        
	        WebElement wereferrer=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[3]/label/input"));
	        WebElement weadvisor1=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[1]/label/input"));
	        WebElement weprovider=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[2]/label/input"));
	        if (!wereferrer.isSelected()) {
				wereferrer.click();
				Sleeper.sleepTightInSeconds(5);
             } 
	        if (weadvisor1.isSelected()) {
				weadvisor1.click();
				Sleeper.sleepTightInSeconds(5);
             } 
	        if (weprovider.isSelected()) {
				weprovider.click();
				Sleeper.sleepTightInSeconds(5);
             } 
	        d.findElement(By.linkText("Integration")).click();
	        d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	        
	        Sleeper.sleepTightInSeconds(5);
	        d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div[1]/div[2]/div[1]/p/a")).click();
			 Sleeper.sleepTightInSeconds(2);
			 java.util.Set<String> winna=d.getWindowHandles();
			 Object[] windows=winna.toArray();
			 String win1=windows[0].toString();
			 String win2=windows[1].toString();
			 d.switchTo().window(win2);
			 Sleeper.sleepTightInSeconds(2);
			 String headingname=d.findElement(By.xpath("html/body/div[3]/div/div/ul/li[3]/a/div/h5")).getText();
			
			  
			 d.findElement(By.id("firstName")).sendKeys(fname);
			  
			  d.findElement(By.id("lastName")).sendKeys(lname);
			 
			  d.findElement(By.id("emailAddress")).sendKeys(email);
			  
			  d.findElement(By.id("password")).sendKeys(pass);
			  
			  d.findElement(By.id("confirmPassword")).sendKeys(cpass);
			  d.findElement(By.id("nextButton")).click();
			  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			  Sleeper.sleepTightInSeconds(5);
			  
			  d.findElement(By.id("dateOfBirth")).sendKeys(DOB);
				 
			  d.findElement(By.id("SSN")).sendKeys(SSN);
			 
			  Select ms=new Select(d.findElement(By.id("maritalStatus")));
			  ms.selectByVisibleText(maritalstatus);
			  
			  d.findElement(By.id("phone")).sendKeys(phone);
			  
			  d.findElement(By.id("street")).sendKeys(address1);
			  
			  d.findElement(By.id("aptSte")).sendKeys(address2);
			  
			  d.findElement(By.id("city")).sendKeys(city);
			  
			  Select st=new Select(d.findElement(By.id("state")));
			  st.selectByVisibleText(state);
			  
			  d.findElement(By.id("zip")).sendKeys(zipcode);
			  Sleeper.sleepTightInSeconds(2);
			  
			  Select toa=new Select(d.findElement(By.id("accountType")));
			  toa.selectByVisibleText(typeofaccount);
			  Sleeper.sleepTightInSeconds(2);
			  
			  d.findElement(By.xpath("html/body/div[3]/form/div[6]/div/div[2]/div/div[1]/div/table/tbody/tr[1]/td/h5/label/span")).click();
			  Sleeper.sleepTightInSeconds(2);
			  JavascriptExecutor jd = (JavascriptExecutor)d;
		        jd.executeScript("window.scrollBy(0,+3200)", "");
			
			  d.findElement(By.xpath("html/body/div[3]/form/div[8]/div[2]/div[2]/label/span")).click();
			  d.findElement(By.id("nextButton")).click();
			  Sleeper.sleepTightInSeconds(3);
			  js.executeScript("window.scrollBy(0,+600)", "");
			  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			if (headingname.contains("Account Fees")) {
				d.switchTo().window(win2).close();
				d.switchTo().window(win1);
				return true;
				
			} else {
				d.switchTo().window(win2).close();
				d.switchTo().window(win1);
				return false;
			}
				
				
		} 
		catch (Exception e) 
		{
			d.get(url);
			System.out.println(e);
			return false;
		}
	}
}



