package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;
import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;

public class Sitedata_Add_Fee_details extends NDiraConstantsV2
{
	public static String displayname,Feedetailsname,Feetype,amount,Description;
	public static boolean Sitedata_Add_Fee_detailsTest() throws IOException
	{
		try
		{
			Logout no=new Logout();
		  	no.mLogin();
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText("Site Data")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText("Fee Schedules")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			
		  	d.findElement(By.linkText(displayname)).click();
		  	d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Add Fee Details']")).click();
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  	d.findElement(By.xpath("//input[@type='text' and @placeholder='name'] ")).sendKeys(Feedetailsname);
		  	Select feetype=new Select(d.findElement(By.id("fee-type")));
		  	feetype.selectByVisibleText(Feetype);
		  	d.findElement(By.xpath("//input[@placeholder='$']")).sendKeys(amount);
		  	d.findElement(By.xpath("//textarea[@placeholder='description' and  @ type='text']")).sendKeys(Description);
		  	d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Create'] ")).click();
		  	Sleeper.sleepTightInSeconds(2);
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
		  	d.findElement(By.xpath("//*[@class='fa fa-pencil-square-o']")).click();
		  	
		  	String expmsg,acmsg;
		  	expmsg="Fee Details updated.";
		  	acmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div/div/div/div/div[2]/div[2]/div/div[2]/div/div/div/div/div/div/div/div/span")).getText();
		  	
		  	if (expmsg.contains(acmsg))
		  	{
		  		return true;
			} 
		  	else 
		  	{
		  		return false;
			}	
			
		}
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
