package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class UserSignupwithNDiralogin extends NDiraConstantsV2

{
	public static String fname,lname,email,pass,cpass,DOB,SSN,maritalstatus,phone,address1,address2,city,state,zipcode,typeofaccount,investiment,phonen,cardno,cardExpiryDate,cardName,cardCVCNumber,AccNo;
	public static boolean UserSignupwithNDiralogin() throws IOException
	{
		try
		{
		XLUtilsV2 xl=new XLUtilsV2();
		String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
		String tcsheet="TestCases";
		String tssheet="TestSteps";
		Logout no=new Logout();
		no.mLogin();
		Sleeper.sleepTightInSeconds(5);
		
		d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[2]/a/div[2]")).click();
		Sleeper.sleepTightInSeconds(2);
		String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.linkText(sponsor)).click();
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/div/a[7]")).click();
				
		 Sleeper.sleepTightInSeconds(2);
		 String userurl=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div[1]/div[2]/div[1]/p/a")).getText();
		 Sleeper.sleepTightInSeconds(2);
		 d.get(userurl);
		 Sleeper.sleepTightInSeconds(2);
		  
		 d.findElement(By.id("firstName")).sendKeys(fname);
		  
		  d.findElement(By.id("lastName")).sendKeys(lname);
		 
		  d.findElement(By.id("emailAddress")).sendKeys(email);
		  
		  d.findElement(By.id("password")).sendKeys(pass);
		  
		  d.findElement(By.id("confirmPassword")).sendKeys(cpass);
		  d.findElement(By.id("nextButton")).click();
		  Sleeper.sleepTightInSeconds(2);
		 
		  d.findElement(By.id("dateOfBirth")).sendKeys(DOB);
		 
		  d.findElement(By.id("SSN")).sendKeys(SSN);
		 
		  Select ms=new Select(d.findElement(By.id("maritalStatus")));
		  ms.selectByVisibleText(maritalstatus);
		  
		  d.findElement(By.id("phone")).sendKeys(phone);
		  
		  d.findElement(By.id("street")).sendKeys(address1);
		  
		  d.findElement(By.id("aptSte")).sendKeys(address2);
		  
		  d.findElement(By.id("city")).sendKeys(city);
		  
		  Select st=new Select(d.findElement(By.id("state")));
		  st.selectByVisibleText(state);
		  
		  d.findElement(By.id("zip")).sendKeys(zipcode);
		  Sleeper.sleepTightInSeconds(2);
		  
		  Select toa=new Select(d.findElement(By.id("accountType")));
		  toa.selectByVisibleText(typeofaccount);
		  Sleeper.sleepTightInSeconds(2);
		  d.findElement(By.xpath("html/body/div[3]/form/div[6]/div/div[2]/div/div[1]/div/table/tbody/tr[1]/td/h5/label/span")).click();
		  Sleeper.sleepTightInSeconds(2);
		  d.findElement(By.xpath("html/body/div[3]/form/div[7]/div[2]/div[2]/label/span")).click();
		  d.findElement(By.id("nextButton")).click();
		  Sleeper.sleepTightInSeconds(3);
		  
		  d.findElement(By.id("investmentAmount")).sendKeys(investiment);
		  Sleeper.sleepTightInSeconds(3);
		  d.findElement(By.xpath("html/body/div[3]/form/div[1]/div[4]/div[2]/label/span")).click();
		  Sleeper.sleepTightInSeconds(3);
		  d.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label/span")).click();
		  Sleeper.sleepTightInSeconds(2);
		  
		  d.findElement(By.id("contactMePhone")).sendKeys(phonen);
		  Sleeper.sleepTightInSeconds(3);
		  d.findElement(By.xpath("html/body/div[3]/form/div[3]/div[2]/div[2]/label[1]/span")).click();
		  Sleeper.sleepTightInSeconds(2);
		  d.findElement(By.xpath("html/body/div[3]/form/div[4]/div[2]/div[2]/label/span")).click();
		  Sleeper.sleepTightInSeconds(2);
		  d.findElement(By.id("nextButton")).click();
		  
		  d.findElement(By.id("cardNumber")).sendKeys(cardno);
		  
		  d.findElement(By.id("cardExpiryDate")).sendKeys(cardExpiryDate);
		  
		  d.findElement(By.id("cardName")).sendKeys(cardName);
		  
		  d.findElement(By.id("cardCVCNumber")).sendKeys(cardCVCNumber);
		  Sleeper.sleepTightInSeconds(4);
		  d.findElement(By.xpath("html/body/div[3]/form/div[5]/div[2]/div[2]/label[1]/span")).click();
		  Sleeper.sleepTightInSeconds(4);
		  d.findElement(By.xpath("html/body/div[3]/form/div[7]/div[2]/div[2]/label/span[1]")).click();
		  Sleeper.sleepTightInSeconds(2);
		  d.findElement(By.id("nextButton")).click();
		  
		 
		  AccNo=d.findElement(By.xpath("html/body/div[4]/div/div/div[2]/div/p[2]")).getText();
		  //xl.setCellData(xlfile, tssheet, 13, 26, AccNo);
		  
		  String exp;
		  exp=AccNo;
			  
			  if (exp.contains(exp))
			   {
			 	return true;
			   } 
			  
			   else 
			   {
			 	  return false;
			   }
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
		  
	 }
}
