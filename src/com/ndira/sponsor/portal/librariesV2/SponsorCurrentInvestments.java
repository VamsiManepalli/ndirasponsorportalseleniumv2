package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class SponsorCurrentInvestments extends NDiraConstantsV2
{
	public static boolean SponsorCurrentInvestigations() throws IOException
	{
		try 
		{
	  	Sponsorlogout no=new Sponsorlogout();
	  	no.mLogin();
	  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  	d.findElement(By.linkText("Current Investments")).click();
	  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/div/div/div/form/ul/li[1]/button")).click();
	  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  	d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/button[2]")).click();
	  	Sleeper.sleepTightInSeconds(2);
	  	String expres;
	  	expres=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/p")).getText();
	  	if (expres.contains("No investments found.")) 
	  	{
	  		return true;
		} 
	  	else 
	  	{
	  		return false;
		}
		
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	  	
	}
}
