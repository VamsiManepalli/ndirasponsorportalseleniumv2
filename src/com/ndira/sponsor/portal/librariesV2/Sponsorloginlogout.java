package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class Sponsorloginlogout extends NDiraConstantsV2
{
	public static String email,password;
	public boolean Login(String email,String password) throws IOException
	{
		try
		{
		  XLUtilsV2 xl=new XLUtilsV2();
		  String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
	      String tcsheet="TestCases";
		  String tssheet="TestSteps";
		  d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[2]/input")).sendKeys(email);
		  d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[3]/input")).sendKeys(password);
		  d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[4]/div[2]/button")).click();
		  String expmsg,actmsg;
		  expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[2]")).getText();
		  actmsg=xl.getCellData(xlfile, tssheet, 1, 5);
		   if (expmsg.equalsIgnoreCase(actmsg))
		   {
			   d.findElement(By.linkText("Log out")).click();
			   return true;
		   } 
		   else
		   {
			   return false;
		   }
		
		}
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
	  
	   }
  

}
