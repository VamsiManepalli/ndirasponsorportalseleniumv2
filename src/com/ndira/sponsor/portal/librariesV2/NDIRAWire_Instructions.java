package com.ndira.sponsor.portal.librariesV2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class NDIRAWire_Instructions extends NDiraConstantsV2
{
	public static String BankName,Address,City,State,Zip,accname,AccountNumber,RoutingNumber;
	public boolean NDIRAWire_InstructionsTest()
	{
		try
		{
			XLUtilsV2 xl=new XLUtilsV2();
			String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
		  	Logout no=new Logout();
		  	no.mLogin();
		  	
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
			d.findElement(By.linkText("Sponsors")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
			String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
			d.findElement(By.linkText(sponsor)).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
			d.findElement(By.linkText("Wire Instructions")).click();
						
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Sleeper.sleepTightInSeconds(2);
			
			d.findElement(By.xpath("//*[contains(@ng-model,'bankName')]")).clear();
			d.findElement(By.xpath("//*[contains(@ng-model,'address')]")).clear();
			d.findElement(By.xpath("//*[contains(@ng-model,'city')]")).clear();
			d.findElement(By.xpath("//*[contains(@ng-model,'state')]")).clear();
			d.findElement(By.xpath("//*[contains(@ng-model,'zip')]")).clear();
			d.findElement(By.xpath("//*[contains(@ng-model,'accountHolderName')]")).clear();
			d.findElement(By.xpath("//*[contains(@ng-model,'accountNumber')]")).clear();
			d.findElement(By.xpath("//*[contains(@ng-model,'routingNumber')]")).clear();
			
			d.findElement(By.xpath("//*[contains(@ng-model,'bankName')]")).sendKeys(BankName);
			d.findElement(By.xpath("//*[contains(@ng-model,'address')]")).sendKeys(Address);
			d.findElement(By.xpath("//*[contains(@ng-model,'city')]")).sendKeys(City);
			d.findElement(By.xpath("//*[contains(@ng-model,'state')]")).sendKeys(State);
			d.findElement(By.xpath("//*[contains(@ng-model,'zip')]")).sendKeys(Zip);
			d.findElement(By.xpath("//*[contains(@ng-model,'accountHolderName')]")).sendKeys(accname);
			d.findElement(By.xpath("//*[contains(@ng-model,'accountNumber')]")).sendKeys(AccountNumber);
			d.findElement(By.xpath("//*[contains(@ng-model,'routingNumber')]")).sendKeys(RoutingNumber);
			d.findElement(By.xpath("//btn[contains(@class, 'primary')]")).click();
			Sleeper.sleepTightInSeconds(2);
			Runtime.getRuntime().exec("resources\\PDFclose.exe");
			JavascriptExecutor js = (JavascriptExecutor)d;
			Sleeper.sleepTightInSeconds(2);
			String expmsg,acmsg;
			expmsg="WireInstructions updated successfully.";
			acmsg=d.findElement(By.xpath("//p[@class='alert alert-info ng-binding']")).getText();
			if (expmsg.equalsIgnoreCase(acmsg)) 
			{
				return true;
			} 
			else 
			{
				return false;
			}
		
		} 
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
	
	}
}
