package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;



public class SponsorAddTeamMember extends NDiraConstantsV2
{
	public static String fname,lname,email,role;
	public static boolean SponsorAddTeamMember() throws IOException
	{
		try
		{
			Sponsorlogout no=new Sponsorlogout();
		  	no.mLogin();
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  	d.findElement(By.linkText("Manage Team")).click();
		  
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.id("new-first-name")).sendKeys(fname);
			d.findElement(By.id("new-last-name")).sendKeys(lname);
			d.findElement(By.id("new-email")).sendKeys(email);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div/div/div[1]/div/form/div[4]/div/button")).click();
			Sleeper.sleepTightInSeconds(4);
			Select role1=new Select(d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div/div/div[2]/div/table/tbody/tr[2]/td[3]/select")));
			role1.selectByVisibleText(role);
			String expmsg,acmsg;
			expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div/div/div[1]/div/p")).getText();
			acmsg="Account created and password reset email sent.";
			if (acmsg.equalsIgnoreCase(expmsg))
			{
				return true;
			} 
			else 
			{
				return false;
			}
			
		}
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
