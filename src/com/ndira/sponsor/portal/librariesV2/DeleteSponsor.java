package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class DeleteSponsor extends NDiraConstantsV2
{
	public static String company,Category,fname,lname,email,number,Howdiduknow,details,password,cpass;
	public static boolean DeleteSponsorTest() throws IOException
	{
		try
		{		
			XLUtilsV2 xl=new XLUtilsV2();
			String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/div/div/a")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.id("company")).sendKeys(company);
			Select cat=new Select(d.findElement(By.id("primary-investment-category")));
			cat.selectByVisibleText(Category);
			
			d.findElement(By.id("name-first")).sendKeys(fname);
			
			d.findElement(By.id("name-last")).sendKeys(lname);
			
			d.findElement(By.id("email")).sendKeys(email);
			
			d.findElement(By.id("phone-1")).sendKeys(number);
			
			Select howdiduknow=new Select(d.findElement(By.id("sponsor-how-did-you-know")));
			howdiduknow.selectByVisibleText(Howdiduknow);
			
			if ((Howdiduknow.equalsIgnoreCase("Event"))||(Howdiduknow.equalsIgnoreCase("Investment Provider/Financial Advisor"))||(Howdiduknow.equalsIgnoreCase("New Direction Trust Company Representative"))||(Howdiduknow.equalsIgnoreCase("Wholesaler/Depository"))||(Howdiduknow.equalsIgnoreCase("Other")))
			{
				d.findElement(By.id("additional-details")).sendKeys(details);
			} 
			
			d.findElement(By.id("password")).sendKeys(password);
			
			d.findElement(By.id("password-again")).sendKeys(cpass);
			
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[9]/div/div/button")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			//Sponsor logout
			d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
			
			//NDTCO login
			Logout no=new Logout();
			no.mLogin();
			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
			d.findElement(By.linkText("Sponsors")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String sponsor=xl.getCellData(xlfile, tssheet, 151, 5);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText(sponsor)).click();
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/ul/li[2]/button")).click();
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/button[2]")).click();
			return true;
			
		} 
		catch (Exception e)
		{
			System.out.println(e);
			d.get(url);
			return false;
		}
		
	}
}
