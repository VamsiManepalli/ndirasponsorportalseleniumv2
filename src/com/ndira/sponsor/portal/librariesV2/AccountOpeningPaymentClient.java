package com.ndira.sponsor.portal.librariesV2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class AccountOpeningPaymentClient extends NDiraConstantsV2
{
	public static String asponsorname,fname,lname,email,pass,cpass,DOB,SSN,maritalstatus,phone,address1,address2,city,state,zipcode,typeofaccount,Amount;
	public static boolean AccountOpeningPaymentClientTest() 
	{

		try
		{
			XLUtilsV2 xl=new XLUtilsV2();
			String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			
			Logout no=new Logout();
		  	no.mLogin();
		  	
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
			d.findElement(By.linkText("Sponsors")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText(sponsor)).click();
			
			//Sponsor relationship
	        WebElement advisor=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[1]/label/input"));
	        WebElement provider=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[2]/label/input"));
	        WebElement referrer=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[3]/label/input"));
	        
	        if (!advisor.isSelected()) {				
				advisor.click();				
             } 
	        Sleeper.sleepTightInSeconds(2);
	        d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	        if (provider.isSelected()) {				
				provider.click();				
             } 
	        Sleeper.sleepTightInSeconds(2);
	        d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	        if (referrer.isSelected()) {				
				referrer.click();
			} 
	        Sleeper.sleepTightInSeconds(2);
	        d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	        
	        //Account Opening Payment
			WebElement Client=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[3]/div[2]/div[1]/label/input"));
			if (Client.isSelected()) 
			{
				Client.click();
			}
			 Sleeper.sleepTightInSeconds(2);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			WebElement prepayment=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[3]/div[2]/div[2]/label/input"));
			if (!prepayment.isSelected()) 
			{
				prepayment.click();
			}
			 Sleeper.sleepTightInSeconds(2);
			 d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			
			 JavascriptExecutor j = (JavascriptExecutor)d;
		     j.executeScript("window.scrollBy(0,-1600)", "");
		     Sleeper.sleepTightInSeconds(2);
			//Investment Components				
			d.findElement(By.linkText("Investment Components")).click();
			Sleeper.sleepTightInSeconds(2);			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div[1]/label/input")).click();
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/button")).click();
			Sleeper.sleepTightInSeconds(2);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);				
			//Integration
			d.findElement(By.linkText("Integration")).click();
			Sleeper.sleepTightInSeconds(3);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div[1]/div[2]/div[1]/p/a")).click();
			Sleeper.sleepTightInSeconds(2);
			java.util.Set<String> winna=d.getWindowHandles();
			 Object[] windows=winna.toArray();
			 String win1=windows[0].toString();
			 String win2=windows[1].toString();
			 d.switchTo().window(win2);		
			 Sleeper.sleepTightInSeconds(3);	
			 d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			 d.findElement(By.id("firstName")).sendKeys(fname);			  
			 d.findElement(By.id("lastName")).sendKeys(lname);			 
			 d.findElement(By.id("emailAddress")).sendKeys(email);			  
			 d.findElement(By.id("password")).sendKeys(pass);
			 d.findElement(By.id("confirmPassword")).sendKeys(cpass);
			 d.findElement(By.id("nextButton")).click();
			 d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			 Sleeper.sleepTightInSeconds(3);			  
			 d.findElement(By.id("dateOfBirth")).sendKeys(DOB);				 
			 d.findElement(By.id("SSN")).sendKeys(SSN);			 
			 Select ms=new Select(d.findElement(By.id("maritalStatus")));
			 ms.selectByVisibleText(maritalstatus);			  
			 d.findElement(By.id("phone")).sendKeys(phone);			  
			 d.findElement(By.id("street")).sendKeys(address1);			  
			 d.findElement(By.id("aptSte")).sendKeys(address2);			  
			 d.findElement(By.id("city")).sendKeys(city);			  
			 Select st=new Select(d.findElement(By.id("state")));
			 st.selectByVisibleText(state);			  
			 d.findElement(By.id("zip")).sendKeys(zipcode);
			  			  
			 Select toa=new Select(d.findElement(By.id("accountType")));
			 toa.selectByVisibleText(typeofaccount);			  
			  
			 JavascriptExecutor js = (JavascriptExecutor)d;
		     js.executeScript("window.scrollBy(0,+3200)", "");
			
			 d.findElement(By.xpath("html/body/div[3]/form/div[8]/div[2]/div[2]/label/span")).click();
			 d.findElement(By.id("nextButton")).click();
			  
			 d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			 
			 d.findElement(By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/label/span")).click();
			 d.findElement(By.id("investmentAmount")).sendKeys(Amount);
			 
			 Select depository=new Select(d.findElement(By.id("depositoryChoice")));
			 depository.selectByIndex(1);
			 
			 Select depositoryType=new Select(d.findElement(By.id("depositoryType")));
			 depositoryType.selectByIndex(1);
			 
			 d.findElement(By.xpath("html/body/div[3]/div[2]/div[2]/div[2]/label[1]/span")).click();
			 d.findElement(By.xpath("html/body/div[3]/div[4]/div[2]/div[2]/label/span")).click();			
			 d.findElement(By.id("nextButton")).click();
			 Sleeper.sleepTightInSeconds(2);
			 d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			 String expres,acres;
			 expres=d.findElement(By.xpath("html/body/div[3]/form/div[3]/div[1]/div[2]/span")).getText();
			 acres="Paid By";
			 
			if (expres.contains(acres))
			{			
				d.switchTo().window(win2).close();
				d.switchTo().window(win1);
				return true;
			} 
			else
			{
				d.switchTo().window(win2).close();
				d.switchTo().window(win1);
				return false;
			}
				
		}
		catch (Exception e)
		{
			d.get(url);
			System.out.println(e);
			return false;
		}
	}
}
