package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class SponsorUsersignupNegativeStep2 extends SponsorUsersignupNegativeStep1
{
	public static String fname,lname,email,pass,cpass,DOB,SSN,maritalstatus,phone,address1,address2,city,state,zipcode,typeofaccount;
	public static boolean SponsorUsersignupNegativeStep2() throws IOException 
	{
	 try
	 {
		 XLUtilsV2 xl=new XLUtilsV2();
		 String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
		 String tcsheet="TestCases";
		 String tssheet="TestSteps";
	
		  SponsorUsersignupNegativeStep1 sus1=new SponsorUsersignupNegativeStep1();
		  sus1.SponsorUsersignupNegativeStep1();
		  
		  Sleeper.sleepTightInSeconds(2);
		  
		  d.findElement(By.id("dateOfBirth")).sendKeys(DOB);
		  
		  d.findElement(By.id("SSN")).sendKeys(SSN);
		
		  Select ms=new Select(d.findElement(By.id("maritalStatus")));
		  ms.selectByVisibleText(maritalstatus);
		  
		  d.findElement(By.id("phone")).sendKeys(phone);
		  
		  d.findElement(By.id("street")).sendKeys(address1);
		  
		  d.findElement(By.id("aptSte")).sendKeys(address2);
		 
		  d.findElement(By.id("city")).sendKeys(city);
		 
		  Select st=new Select(d.findElement(By.id("state")));
		  st.selectByVisibleText(state);
		  
		  d.findElement(By.id("zip")).sendKeys(zipcode);
		  Sleeper.sleepTightInSeconds(2);
		 
		  Select toa=new Select(d.findElement(By.id("accountType")));
		  toa.selectByVisibleText(typeofaccount);
		  Sleeper.sleepTightInSeconds(2);
		  d.findElement(By.xpath("html/body/div[3]/form/div[6]/div/div[2]/div/div[1]/div/table/tbody/tr[1]/td/h5/label/span")).click();
		  Sleeper.sleepTightInSeconds(2);
		  d.findElement(By.xpath("html/body/div[3]/form/div[7]/div[2]/div[2]/label/span")).click();
		  Sleeper.sleepTightInSeconds(2);
		  d.findElement(By.id("nextButton")).click();
		  
		  String expurl;
		  expurl=d.getCurrentUrl();
		  if (expurl.contains("https://app.ndiraplatform-uat.com/admin-new/sponsor/5b2a4d383152897be33a1bf9/integration"))
		  {
			  return false;	
		  }
		  else
		  {
			  return true;
		  }
		
		} 
	 catch (Exception e)
	 {
		System.out.println(e);
		return false;
	 }
	}
}
