package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class SponsorMessageToNDira extends NDiraConstantsV2
{
	public static String text;
	public static boolean SponsorMessageToNDira() throws IOException
	{
		try 
		{		
		    Sponsorlogout no=new Sponsorlogout();
		    no.mLogin();
		    d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		    d.findElement(By.linkText("Messages")).click();	  
			d.findElement(By.id("message-body")).sendKeys(text);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/div/div/div[1]/div/form/div[2]/div/button")).click();
			String expmsg;
			Sleeper.sleepTightInSeconds(2);
			expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/div/div/div[1]/div/p[1]")).getText();
			if (expmsg.equalsIgnoreCase("Message sent successfully.")) 
			{
				return true;
			} 
			else
			{
				return false;
			}
			
			}
			catch (Exception e)
			{
				System.out.println(e);
				return false;
			}
	}
}
