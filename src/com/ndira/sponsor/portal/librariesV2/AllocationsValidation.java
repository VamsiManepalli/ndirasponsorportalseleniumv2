package com.ndira.sponsor.portal.librariesV2;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class AllocationsValidation extends NDiraConstantsV2
{
	public static String Days,Min,Max;
	public static boolean SponsorAllocationsValidationTest() 
	{
		try 
		{
			XLUtilsV2 xl=new XLUtilsV2();
			  String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
			  String tcsheet="TestCases";
			  String tssheet="TestSteps";
			  Logout no=new Logout();
			  no.mLogin();			
			  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
			  d.findElement(By.linkText("Sponsors")).click();
			  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			  String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
			  d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
			  d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
			  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			  d.findElement(By.linkText(sponsor)).click();
			  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
				//To clear the data
				List<WebElement> li=d.findElements(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div[2]/div/input"));
				
				for (int i = 0; i <li.size()-1; i++) 
				{
					li.get(i).clear();	
					
				}						
				Sleeper.sleepTightInSeconds(2);				
				d.findElement(By.id("allocation-expiration")).sendKeys(Days);
				d.findElement(By.id("allocation-minimum")).sendKeys(Min);
				d.findElement(By.id("allocation-maximum")).sendKeys(Max);
				
				d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div[2]/ul/li/button")).click();
				/*List<WebElement> list=d.findElements(By.xpath("//button[text()='Save']"));
				for (int i = 0; i <= list.size(); i++)
				{
					list.get(1).click();	
				}*/
				
				String acmsg;				
				acmsg=d.findElement(By.xpath("//*[@class='ng-binding ng-scope']")).getText();
				
				if (AllocationsList.getAllocationsList().contains(acmsg)) 
				{
					return true;
				}
				else 
				{
					return false;
				}
				
		}
		catch (Exception e) 
		{
			System.out.println("hi");
			System.out.println(e);
			return false;
		}
	}
}
