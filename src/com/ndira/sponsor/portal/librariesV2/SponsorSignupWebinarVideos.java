package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;


public class SponsorSignupWebinarVideos extends NDiraConstantsV2
{
	public static boolean SponsorSignupWebinarVideo() throws IOException
	{
		try 
		{
		  Sponsorlogout.mLogin();
		  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  d.findElement(By.linkText("Webinar Videos")).click();
		  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  String exptxt;
		  exptxt=d.findElement(By.xpath(".//*[@id='resources-webinar-videos']/div[1]/div/h1")).getText();
		  if (exptxt.equalsIgnoreCase("Webinar Videos"))
		  {
			return true;
		  } 
		  else
		  {
			  return false;
		  }
			
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
