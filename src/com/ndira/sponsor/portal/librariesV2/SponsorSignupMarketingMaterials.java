package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
public class SponsorSignupMarketingMaterials extends NDiraConstantsV2
{
	public static boolean SponsorSignupMarketingMaterials() throws IOException
	{
		try 
		{
		  Sponsorlogout no=new Sponsorlogout();
		  no.mLogin();
		  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  d.findElement(By.linkText("Marketing Materials")).click();
		  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  String exptxt;
		  exptxt=d.findElement(By.xpath(".//*[@id='resources-marketing-materials']/div/div/div[1]/div/h1")).getText();
		  
		  if (exptxt.equalsIgnoreCase("Marketing Materials")) 
		  {
			return true;
		  } 
		  else
		  {
			  return false;
		  }
			
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
	
}
