package com.ndira.sponsor.portal.librariesV2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;


public class SponsorWireInstructions extends NDiraConstantsV2
{
	public static String BankName,Address,City,State,Zip,accname,AccountNumber,RoutingNumber;
	public static boolean SponsorWireInstructionsTest() 
	{
		try 
		{			
			Sponsorlogout no=new Sponsorlogout();
			no.mLogin();
			
			d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			d.findElement(By.linkText("Wire Instructions")).click();			
			d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			//To clear the data
			List<WebElement> li=d.findElements(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[2]/div/form/div/div/input"));
			
			for (int i = 0; i <li.size(); i++) 
			{
				li.get(i).clear();		
			}					
			d.findElement(By.xpath("//*[contains(@ng-model,'bankName')]")).sendKeys(BankName);
			d.findElement(By.xpath("//*[contains(@ng-model,'address')]")).sendKeys(Address);
			d.findElement(By.xpath("//*[contains(@ng-model,'city')]")).sendKeys(City);
			d.findElement(By.xpath("//*[contains(@ng-model,'state')]")).sendKeys(State);
			d.findElement(By.xpath("//*[contains(@ng-model,'zip')]")).sendKeys(Zip);
			d.findElement(By.xpath("//*[contains(@ng-model,'accountHolderName')]")).sendKeys(accname);
			d.findElement(By.xpath("//*[contains(@ng-model,'accountNumber')]")).sendKeys(AccountNumber);
			d.findElement(By.xpath("//*[contains(@ng-model,'routingNumber')]")).sendKeys(RoutingNumber);
			
			d.findElement(By.xpath("//btn[contains(@class, 'primary')]")).click();
			Sleeper.sleepTightInSeconds(2);
			Runtime.getRuntime().exec("resources\\PDFclose.exe");
			Sleeper.sleepTightInSeconds(5);
			String expmsg,acmsg;
			acmsg=d.findElement(By.xpath("//p[@class='alert alert-info ng-binding']")).getText();
			expmsg="WireInstructions updated.";
			if (acmsg.equalsIgnoreCase(expmsg))
			{
				return true;
			} 
			else 
			{				
				return false;
			}
			
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
