package com.ndira.sponsor.portal.librariesV2;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;

public class NDTCOUsersNegative extends NDiraConstantsV2
{
	public static String fname,lname,email;
	public static boolean NDTCOUsersNegativeTest()
	{
		try
		{
			Logout out = new Logout();
			out.mLogin();
			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText("NDTCO Users")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			//Reading the table data
			WebElement table;
			table=d.findElement(By.id("admin-ndira-users"));
			List<WebElement> rows,cols;
			rows=table.findElements(By.tagName("tr"));
			int rc=rows.size();
			cols=rows.get(rc-1).findElements(By.tagName("td"));
			String Acdata=cols.get(0).getText();			
			
			d.findElement(By.id("new-first-name")).sendKeys(fname);
			d.findElement(By.id("new-last-name")).sendKeys(lname);
			d.findElement(By.id("new-email")).sendKeys(email);
			d.findElement(By.xpath(".//*[@id='admin-ndira-users']/div[3]/div[1]/form/div[4]/div/button")).click();			
			//Refresh		
			d.navigate().refresh();
			//Reading the table data			
			WebElement table1;
			table1=d.findElement(By.id("admin-ndira-users"));
			List<WebElement> rows1,cols1;
			rows1=table1.findElements(By.tagName("tr"));
			int rc1=rows1.size();
			cols1=rows1.get(rc1-1).findElements(By.tagName("td"));
			String expdata=cols1.get(0).getText();
			
			if (Acdata.equalsIgnoreCase(expdata))
			{
				return true;
			}
			else
			{
				return false;
			}
			
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
