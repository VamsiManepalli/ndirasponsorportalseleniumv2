package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class Sponsormessages extends NDiraConstantsV2
{
	public static String message;
	public static boolean sponsermessages() throws IOException
	{
		try
		{
		XLUtilsV2 xl=new XLUtilsV2();
		String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
		String tcsheet="TestCases";
		String tssheet="TestSteps";
		Logout no=new Logout();
		no.mLogin();
		
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
		d.findElement(By.linkText("Sponsors")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.linkText(sponsor)).click();
		
		d.findElement(By.linkText("Messages")).click();
		
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
		d.findElement(By.id("message-body")).sendKeys(message);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div[2]/div/div/div[1]/div/form/div[2]/div/button")).click();
		Sleeper.sleepTightInSeconds(2);
		String expmsg,acmsg;
		expmsg="Message sent successfully.";
		
		acmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div[2]/div/div/div[1]/div/p[1]")).getText();
		Sleeper.sleepTightInSeconds(2);
		//System.out.println(acmsg);
		if (acmsg.equalsIgnoreCase(expmsg))
		{
			return true;	
		} 
		else 
		{
			return false;
		}
	  } 
	  catch (Exception e)
	  {
		System.out.println(e);
		return false;
	  }
	}
}
