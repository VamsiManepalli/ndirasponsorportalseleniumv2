package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class SponsorInvestments extends NDiraConstantsV2
{
	public static boolean SponsorInvestiments() throws IOException
	{
		try
		{
		  XLUtilsV2 xl=new XLUtilsV2();
		  String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
		  String windows_component = NdiraUtilsV2.getProperty("file.windows.component");
		  String tcsheet="TestCases";
		  String tssheet="TestSteps";
		  Logout no=new Logout();
		  no.mLogin();
		  
		  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  d.findElement(By.linkText("Sponsors")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.linkText(sponsor)).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.linkText("Investments")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div[2]/div/div[1]/div/label[1]")).click();
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.className("upload-area")).click();
		Sleeper.sleepTightInSeconds(5);
		Runtime.getRuntime().exec(windows_component);
		//d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div[2]/div/div[1]/div/label[1]")).click();
		String expres;
		expres=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[1]/div/div")).getText();
		Sleeper.sleepTightInSeconds(2);
		if (expres.contains("Sponsor successfully updated."))
		{
			return true;		
		} 
		else
		{
			return false;
		}

	} 
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
		
	}
}
