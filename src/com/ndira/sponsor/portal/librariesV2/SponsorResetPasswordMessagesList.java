package com.ndira.sponsor.portal.librariesV2;

import java.util.ArrayList;
import java.util.List;

public class SponsorResetPasswordMessagesList
{
	public static List<String> getSponsorResetPasswordMessagesList() 
	{
		List<String> list = new ArrayList<>();
		list.add("Please enter your current password.");
		list.add("Please enter a new password, new password (again), and make sure they match.");
		list.add("Password could not be updated - double check current password.");
		return list;
	}
}
