package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class NDIRAUsersignupNegativeStep3 extends NDIRAUsersignupNegativeStep2
{
	public static String fname,lname,email,pass,cpass,DOB,SSN,maritalstatus,phone,address1,address2,city,state,zipcode,typeofaccount,cardno,cardExpiryDate,cardName,cardCVCNumber;
	public static boolean NDIRAUsersignupNegativeStep3() throws IOException 
	{
		try
		{
		  XLUtilsV2 xl=new XLUtilsV2();
		  String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
		  String tcsheet="TestCases";
		  String tssheet="TestSteps";
		  
		  SponsorUsersignupNegativeStep2 sus2=new SponsorUsersignupNegativeStep2();
		  sus2.SponsorUsersignupNegativeStep2();
		  
		  d.findElement(By.id("cardNumber")).sendKeys(cardno);
		  
		  d.findElement(By.id("cardExpiryDate")).sendKeys(cardExpiryDate);
		 
		  d.findElement(By.id("cardName")).sendKeys(cardName);
		 
		  d.findElement(By.id("cardCVCNumber")).sendKeys(cardCVCNumber);
		  
		  Sleeper.sleepTightInSeconds(4);
		  
		  d.findElement(By.xpath("html/body/div[3]/form/div[5]/div[2]/div[2]/label[1]/span")).click();
		  
		  
		  
		  Sleeper.sleepTightInSeconds(4);
		  d.findElement(By.xpath("html/body/div[3]/form/div[7]/div[2]/div[2]/label/span[1]")).click();
		  d.findElement(By.id("nextButton")).click();
		  String expurl;
		  expurl=d.getCurrentUrl();
		  if (expurl.contains(expurl))
		 {
			return true;	
		 }
		else
		{
			return false;
		}	
	  } 
	  catch (Exception e)
		{
		  System.out.println(e);
		return false;
		}
	}
}
