package com.ndira.sponsor.portal.librariesV2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class ForgetPassword extends NDiraConstantsV2
{
	public static String email;
	public static boolean Forgetpassword()
	{
		try 
		{
		  d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[4]/div[1]/ul/li/a")).click();
		  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  d.findElement(By.xpath("html/body/div[1]/div/div/main/div/div/div/div/form/div[3]/div[1]/input")).sendKeys(email);
		  d.findElement(By.xpath("html/body/div[1]/div/div/main/div/div/div/div/form/div[3]/div[2]/button")).click();
		  Sleeper.sleepTightInSeconds(2);		 
		  String expmsg,acmsg;
		  expmsg=d.findElement(By.xpath(".//*[@id='forgotPassword']/div/div/div/div/form/div[2]/div")).getText();
		  d.get(url);
		  if (expmsg.equalsIgnoreCase("Email Sent! Check your email for a link to reset your password."))
		  {
			return true;			
		  } 
		  else
		  {
			  return false;
		  }
			
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
