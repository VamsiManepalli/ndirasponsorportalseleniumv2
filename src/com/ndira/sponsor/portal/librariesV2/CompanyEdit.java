package com.ndira.sponsor.portal.librariesV2;

import java.awt.List;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class CompanyEdit extends NDiraConstantsV2
{
	public static String sponsor,saddress1,saddress2,scity,szipcode,state,website,p1,p2;
	public static boolean companyEdit(String address1,String address2,String city,String zipcode,String state,String website,String p1,String p2) throws IOException
	{
		try
		{		
			Logout no=new Logout();
			no.mLogin();
			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
			d.findElement(By.linkText("Sponsors")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText(sponsor)).click();			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.xpath("//div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[1]/button")).click();
			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);			
			
			//clear the data
			java.util.List<WebElement> sponsorprofile=d.findElements(By.xpath("html/body/div[1]/div/div/div[2]/form/div/div[1]/input"));
			for (int i = 1; i < sponsorprofile.size(); i++) 
			{
				sponsorprofile.get(i).clear();				
			}
			
			
			d.findElement(By.xpath("//input[contains(@ng-model, 'address1')]")).sendKeys(address1);			
			d.findElement(By.xpath("//input[contains(@ng-model, 'address2')]")).sendKeys(address2);			
			d.findElement(By.xpath("//input[contains(@ng-model, 'city')]")).sendKeys(city);				
			Select sta=new Select(d.findElement(By.xpath("//select[contains(@ng-model, 'state')]")));
			sta.selectByVisibleText(state);
			d.findElement(By.xpath("//input[contains(@ng-model, 'zip')]")).sendKeys(zipcode);			
			d.findElement(By.xpath("//input[contains(@ng-model, 'website')]")).sendKeys(website);			
			d.findElement(By.xpath("//input[contains(@ng-model, 'phone1')]")).sendKeys(p1);			
			d.findElement(By.xpath("//input[contains(@ng-model, 'phone2')]")).sendKeys(p2);			
			d.findElement(By.xpath("//button[contains(@class, 'btn btn-primary pull-right')]")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String expmsg=d.findElement(By.xpath("//span[contains(@class, 'ng-scope')]")).getText();
			String actmsg="Sponsor details updated";
			if (expmsg.contains(actmsg)) 
			{
				return true;	
			} 
			else 
			{
				return false;
			}
			
		} 
		catch (Exception e)
		{
			d.findElement(By.xpath("html/body/div[1]/div/div/div[3]/button[1]")).click();
			System.out.println(e);
			return false;
		}
	}
}
