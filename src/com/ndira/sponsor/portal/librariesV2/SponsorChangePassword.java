package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class SponsorChangePassword extends NDiraConstantsV2
{
	public static String Cpassword,Npassword,newpasswordagain;
	public static boolean SponsorChangePassword() throws IOException 
	{
		try
		{
			XLUtilsV2 xl=new XLUtilsV2();
			String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			Sponsorlogout no=new Sponsorlogout();
			no.mLogin();
			d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			d.findElement(By.linkText("My Info")).click();
			
			d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[3]/div/form/div[3]/div/input")).sendKeys(Cpassword);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[3]/div/form/div[4]/div/input")).sendKeys(Npassword);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[3]/div/form/div[5]/div/input")).sendKeys(newpasswordagain);
			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[3]/div/form/div[1]/ul/li/btn")).click();
			Sleeper.sleepTightInSeconds(2);
			
			String acmsg;
			acmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[3]/div/form/div[2]/p")).getText();
			
			if (acmsg.equalsIgnoreCase("Password successfully updated."))
			{
				xl.setCellData(xlfile, tssheet, 1, 13, Npassword);
				xl.setCellData(xlfile, tssheet, 1, 14, Npassword);
				return true;
			}
			else 
			{
				return false;
			}
	
		}
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
	}
}
