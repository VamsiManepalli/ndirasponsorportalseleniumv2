package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class SponsorManageTeamRemoveTeamMember extends NDiraConstantsV2
{
	public static boolean RemoveTeamMember() throws IOException
	{
		try 
		{
			Sponsorlogout no=new Sponsorlogout();
			no.mLogin();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText("Manage Team")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String emailid=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div/div/div[2]/div/table/tbody/tr[2]/td[2]")).getText();
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div/div/div[2]/div/table/tbody/tr[2]/td[4]/ul/li[2]/button")).click();
			Sleeper.sleepTightInSeconds(2);
			
			d.switchTo().alert().sendKeys(emailid);
			d.switchTo().alert().accept();
			
			String expmsg,acmsg;
			expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div/div/div[2]/div/p")).getText();
			acmsg="has been removed.";
			if (acmsg.equalsIgnoreCase(acmsg))
			{
				return true;	
			} 
			else 
			{
				return false;
			}	
		
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
