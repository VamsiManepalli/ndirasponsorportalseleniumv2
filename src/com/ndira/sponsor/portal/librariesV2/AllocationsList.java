package com.ndira.sponsor.portal.librariesV2;

import java.util.ArrayList;
import java.util.List;

public class AllocationsList 
{
	public static List<String> getAllocationsList() 
	{
		List<String> list = new ArrayList<>();
		list.add("Sponsor minimum allocation should not be greater than the max allocation.");
		list.add("Please provide a sponsor allocation expiration greater than 1.");
		return list;
	}
}
