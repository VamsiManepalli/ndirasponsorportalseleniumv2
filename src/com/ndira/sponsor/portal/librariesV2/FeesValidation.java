package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;
import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class FeesValidation extends NDiraConstantsV2
{
	public static String asponsorname,name,displayname,feeschedulename,oraclecode,Displaytext,feelist;
	public static boolean FeesValidationTest(String sponsor) throws IOException 
	{
		try 
		{
			Logout no=new Logout();
		  	no.mLogin();
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText("Site Data")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText("Fee Schedules")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			
			d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Add Fee Overview'] ")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath("//input[@type='text' and @placeholder='name'] ")).sendKeys(name);
			d.findElement(By.xpath("//input[@type='text' and @placeholder='display name'] ")).sendKeys(displayname);
			d.findElement(By.xpath("//input[@type='text' and @placeholder='fee schedule name'] ")).sendKeys(feeschedulename);
			d.findElement(By.xpath("//input[@type='number'] ")).sendKeys(oraclecode);
			d.findElement(By.xpath("//textarea[@type='text' and @placeholder='display text'] ")).sendKeys(Displaytext);
			d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Create'] ")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.linkText("Sponsors")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText(sponsor)).click();
			d.findElement(By.linkText("Fees")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Add Fee Schedule']")).click();
			//d.findElement(By.xpath("//*[contains(@class,'btn-secondary')]")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			WebElement fee=d.findElement(By.xpath("//*[contains(@class,'ng-empty')]"));
			Select values = new Select(fee);
			
			List<WebElement> fees=values.getOptions();		
			boolean flag=true;
			
			for (int i = 0; i < fees.size(); i++) 
			{
				feelist=fees.get(i).getText();
				if (feelist.contains(displayname)) 
				{
					return flag=true;
				}
				else 
				{
					flag=false;
				}
			}
			
			if (flag)
			{
				return true;
			} 
			else
			{
				return false;
			}
		} 
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
	}
}
