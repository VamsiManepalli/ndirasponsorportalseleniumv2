package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;

public class SponsorMyInfoUpdate extends NDiraConstantsV2
{
	public static String fname,lname,email;
	public static boolean SponsorMyInfoUpdate() throws IOException
	{
		try 
		{
		Sponsorlogout no=new Sponsorlogout();
		no.mLogin();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.linkText("My Info")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[2]/div/form/div[3]/div/input")).clear();
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[2]/div/form/div[4]/div/input")).clear();
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[2]/div/form/div[5]/div/input")).clear();
		
		
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[2]/div/form/div[3]/div/input")).sendKeys(fname);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[2]/div/form/div[4]/div/input")).sendKeys(lname);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[2]/div/form/div[5]/div/input")).sendKeys(email);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[2]/div/form/div[1]/ul/li/btn")).click();
		
		String expres, acres;
		Sleeper.sleepTightInSeconds(2);
		expres="Contact information successfully updated.";
		acres=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[2]/div/form/div[2]/p")).getText();
		if (expres.equalsIgnoreCase(acres))
		{
			return true;
		} 
		else 
		{
			return false;
		}
		
	} 
	catch (Exception e) 
	{
		System.out.println(e);
		return false;
	}
		
	}
}
