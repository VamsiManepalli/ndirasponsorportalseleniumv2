package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;

public class NDTCOUsers extends NDiraConstantsV2
{
	public static String fname,lname,email;
	public static boolean NDTCOUsersTest() throws IOException
	{
		try 
		{
			Logout out = new Logout();
			out.mLogin();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText("NDTCO Users")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.id("new-first-name")).sendKeys(fname);
			d.findElement(By.id("new-last-name")).sendKeys(lname);
			d.findElement(By.id("new-email")).sendKeys(email);
			d.findElement(By.xpath(".//*[@id='admin-ndira-users']/div[3]/div[1]/form/div[4]/div/button")).click();
			String expmsg,acmsg;
			expmsg="Account created and password reset email sent.";
			acmsg=d.findElement(By.xpath(".//*[@id='admin-ndira-users']/div[2]/div/div/div/div/div/span")).getText();
			if (expmsg.equalsIgnoreCase(acmsg))
			{
				return true;
			} 
			else
			{
				return false;
			}
		} 
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
	}
}
