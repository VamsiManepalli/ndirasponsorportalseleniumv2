package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class NDIRAHowDidYouKnow extends NDiraConstantsV2
{
	public static boolean NDIRAHowDidYouKnow() throws IOException 
	{
		try
		{
		XLUtilsV2 xl=new XLUtilsV2();
		String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
		String tcsheet="TestCases";
		String tssheet="TestSteps";
		Logout no=new Logout();
		no.mLogin();
		
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
		d.findElement(By.linkText("Sponsors")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.linkText(sponsor)).click();
		
		String acl;
		acl=xl.getCellData(xlfile, tssheet, 1, 11);
		Sleeper.sleepTightInSeconds(4);
		Select select = new Select(d.findElement(By.xpath(".//*[@id='admin-new-sponsor-details-overview']/div/div[1]/div[1]/div[2]/div[5]/select")));
		WebElement option = select.getFirstSelectedOption();
		String exp = option.getText();
		
		
		if (acl.equalsIgnoreCase(exp)) 
		{
			boolean flag=true;
			String exp1;
			exp1=xl.getCellData(xlfile, tssheet, 1, 12);
			if ((exp.equalsIgnoreCase("Event"))||(exp.equalsIgnoreCase("Investment Provider/Financial Advisor"))||(exp.equalsIgnoreCase("New Direction Trust Company Representative"))||(exp.equalsIgnoreCase("Wholesaler/Depository"))||(exp.equalsIgnoreCase("Other")))
			{
				JavascriptExecutor js = (JavascriptExecutor)d;
		        js.executeScript("window.scrollBy(0,+600)", "");		        
				Sleeper.sleepTightInSeconds(4);
				
				String acl1=d.findElement(By.id("additionalDetails")).getAttribute("value");
				if (exp1.equalsIgnoreCase(acl1))
				{
					return flag=true;
				} 
				else 
				{					
					return flag=false;				
				}
			} 	
			if (flag) 
			{
				return true;				
			}
			else 
			{
				return false;
			}		
			
		} 
		else 
		{
			return false;
		}

		
		} catch (Exception e) 
		{			
			System.out.println(e);
			return false;
		}
	}
}
