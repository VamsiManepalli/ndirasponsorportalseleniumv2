package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;

public class NDTCOResetPassword extends NDiraConstantsV2
{
	public static String fname,lname,email;
	public boolean NDTCOResetPasswordTest() throws IOException 
	{
		try 
		{
			Logout out = new Logout();
			out.mLogin();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText("NDTCO Users")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.id("new-first-name")).sendKeys(fname);
			d.findElement(By.id("new-last-name")).sendKeys(lname);
			d.findElement(By.id("new-email")).sendKeys(email);
			d.findElement(By.xpath(".//*[@id='admin-ndira-users']/div[3]/div[1]/form/div[4]/div/button")).click();
			WebElement table;
			table=d.findElement(By.id("admin-ndira-users"));
			List<WebElement> rows,cols;
			rows=table.findElements(By.tagName("tr"));
			int rc=rows.size();
			cols=rows.get(rc-1).findElements(By.tagName("td"));
			Sleeper.sleepTightInSeconds(3);
			cols.get(3).click();
			String expmsg,acmsg;
			expmsg="Reset password email sent to user.";
			acmsg=d.findElement(By.xpath(".//*[@id='admin-ndira-users']/div[2]/div/div/div/div[2]/div/span")).getText();
			if (expmsg.equalsIgnoreCase(acmsg))
			{
				return true;
			} 
			else
			{
				return false;
			}
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
