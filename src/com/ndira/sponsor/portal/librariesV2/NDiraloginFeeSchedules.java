package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;


public class NDiraloginFeeSchedules extends NDiraConstantsV2
{
	public static String name,displayname,feeschedulename,oraclecode,Displaytext;
	public static boolean NDiraloginFeeSchedules() throws IOException
	{
		try 
		{
			Logout no=new Logout();
		  	no.mLogin();
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText("Site Data")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText("Fee Schedules")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Add Fee Overview'] ")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath("//input[@type='text' and @placeholder='name'] ")).sendKeys(name);
			d.findElement(By.xpath("//input[@type='text' and @placeholder='display name'] ")).sendKeys(displayname);
			d.findElement(By.xpath("//input[@type='text' and @placeholder='fee schedule name'] ")).sendKeys(feeschedulename);
			d.findElement(By.xpath("//input[@type='number'] ")).sendKeys(oraclecode);
			d.findElement(By.xpath("//textarea[@type='text' and @placeholder='display text'] ")).sendKeys(Displaytext);
			d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Create'] ")).click();
			String expmsg,acmsg;
			expmsg="Fee Schedule updated.";
			acmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div/div/div/div/div[2]/div/div/div/div/div/div/div/span")).getText();
			
			if (expmsg.equalsIgnoreCase(acmsg))
			{
				return true;			
			} 
			else 
			{
				return false;
			}				
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
