package com.ndira.sponsor.portal.librariesV2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class ForgetPasswordNegative extends NDiraConstantsV2
{
	public static String email;
	public static boolean ForgetPasswordNegative() 
	{
	try 
	{			 
	  d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  d.findElement(By.linkText("Forgot Password")).click();
	  d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  d.findElement(By.xpath("html/body/div[1]/div/div/main/div/div/div/div/form/div[3]/div[1]/input")).sendKeys(email);
	  d.findElement(By.xpath("html/body/div[1]/div/div/main/div/div/div/div/form/div[3]/div[2]/button")).click();
	  Sleeper.sleepTightInSeconds(2);
	  String expmsg;
	  expmsg=d.findElement(By.xpath("html/body/div[1]/div/div/main/div/div/div/div/form/div[1]/div")).getText();
	  if (expmsg.contains("Error: user does not exist.")) 
	  {
		return true;
	  } 
	  else
	  {
		  return false;
	  }
	  
	} 
	catch(Exception e) 
	{
		System.out.println(e);
		return false;		
	}
	
}
}
