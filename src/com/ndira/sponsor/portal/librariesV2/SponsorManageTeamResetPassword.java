package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;

public class SponsorManageTeamResetPassword extends NDiraConstantsV2
{
	public static boolean SponsorManageTeamResetPassword() throws IOException
	{
		try 
		{
		  Sponsorlogout no=new Sponsorlogout();
		  no.mLogin();
		  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  d.findElement(By.linkText("Manage Team")).click();
		  Sleeper.sleepTightInSeconds(2);
		  d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div/div/div[2]/div/table/tbody/tr[2]/td[4]/ul/li[1]/button")).click();
		  Sleeper.sleepTightInSeconds(2);
		  String expmsg,acmsg;
		  acmsg="Reset password email sent to user.";
		  expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div/div/div[2]/div/p")).getText();
		  if (expmsg.equalsIgnoreCase(acmsg))
		  {
			return true;
		  }
		  else 
		  {
			  return false;
		  }
			
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
