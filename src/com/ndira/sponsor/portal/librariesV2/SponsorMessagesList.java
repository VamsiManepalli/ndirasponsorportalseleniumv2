package com.ndira.sponsor.portal.librariesV2;
import java.util.ArrayList;
import java.util.List;

public class SponsorMessagesList 
{
	public static List<String> getSponsorMessagesList() 
	{
		List<String> list = new ArrayList<>();
		list.add("Please enter your company name.");
		list.add("Please enter your first and last name.");
		list.add("Please enter a valid email.");
		list.add("Please enter your phone number.");
		list.add("For security, your password must be at least 8 characters in length and be at most 20 characters in length and contain at least 1 lower case character (a-z) and contain at least 1 upper case character (A-Z) and contain at least 1 digit (0-9) and contain one of the following special characters: !, @, ^, $, %, empty space and only include the specified characters");
		list.add("For security, your password must be at least 8 characters in length and contain at least 1 lower case character (a-z) and contain at least 1 digit (0-9) and contain one of the following special characters: !, @, ^, $, %, empty space");
		list.add("For security, your password must be at least 8 characters in length and contain at least 1 lower case character (a-z) and contain at least 1 upper case character (A-Z) and contain one of the following special characters: !, @, ^, $, %, empty space");
		list.add("For security, your password must be at least 8 characters in length and contain at least 1 digit (0-9) and contain one of the following special characters: !, @, ^, $, %, empty space");
		list.add("For security, your password must be at least 8 characters in length and contain one of the following special characters: !, @, ^, $, %, empty space");
		list.add("Please enter a password and ensure your passwords match.");
		list.add("Please provide how did you hear about us.");
		list.add("Please provide additional details.");
		return list;
	}

}
