
package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class AddFeeScheduleinFees extends NDiraConstantsV2
{
	public static String afeeshedulename;
	
	public static boolean addFeeScheduleinFees(String feeschedulename) throws IOException
	{
		try 
		{
			XLUtilsV2 xl=new XLUtilsV2();
			String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			Logout no=new Logout();
			no.mLogin();
			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
			d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[2]/a/div[2]")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText(sponsor)).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			JavascriptExecutor js = (JavascriptExecutor)d;
	        js.executeScript("window.scrollBy(0,+1800)", "");
	        d.findElement(By.linkText("Fees")).click();
	        Sleeper.sleepTightInSeconds(2);
	        d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Add Fee Schedule']")).click();
	        Sleeper.sleepTightInSeconds(2);
	        Select feelist=new Select(d.findElement(By.xpath("//*[contains(@class,'form-control')]")));
	        feelist.selectByVisibleText(feeschedulename);
	        d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Save Fee Schedules']")).click();
	        Sleeper.sleepTightInSeconds(2);
	        d.navigate().refresh();
	        Sleeper.sleepTightInSeconds(5);
	        WebElement feelink=d.findElement(By.linkText(feeschedulename));
	      
			if (feelink.isDisplayed()) {
				return true;
				
			} else {
				return false;
			}
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}




