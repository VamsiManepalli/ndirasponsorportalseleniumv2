package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;



public class NDIRAUsersignupNegativeStep1 extends NDiraConstantsV2
{
	public static String fname,lname,email,pass,cpass;
	public static boolean NDIRAUsersignupNegative() throws IOException
	{
		try 
		{
			XLUtilsV2 xl=new XLUtilsV2();
			String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
		  	Logout no=new Logout();
		  	no.mLogin();
			Sleeper.sleepTightInSeconds(5);
			d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[2]/a/div[2]")).click();
			Sleeper.sleepTightInSeconds(2);
			String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.linkText(sponsor)).click();
			Sleeper.sleepTightInSeconds(4);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/div/a[7]")).click();
			Sleeper.sleepTightInSeconds(2);
			String link=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div[1]/div[2]/div[1]/p/a")).getText();
			//System.out.println(link);
			//xl.setCellData(xlfile, tssheet, 64, 5, link);
			Sleeper.sleepTightInSeconds(2);
	        d.get(link);
	        Sleeper.sleepTightInSeconds(2);
	  	  
	        d.findElement(By.id("firstName")).sendKeys(fname);
	  	  
	        d.findElement(By.id("lastName")).sendKeys(lname);
	  	  
	        d.findElement(By.id("emailAddress")).sendKeys(email);
	  	  
	        d.findElement(By.id("password")).sendKeys(pass);
	  	 
	        d.findElement(By.id("confirmPassword")).sendKeys(cpass);
	  	  
	        d.findElement(By.id("nextButton")).click();
	        Sleeper.sleepTightInSeconds(2);
	       
			String expurl;
			expurl=d.getCurrentUrl();
			if (expurl.contains(expurl))
			{
				return true;	
			}
			else
			{
				return false;
			}
		} 
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}	
	}
}
