package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class InvestmentComponentsClientRelationship extends NDiraConstantsV2
{
	public static String fname,lname,email,pass,cpass,DOB,SSN,maritalstatus,phone,address1,address2,city,state,zipcode,typeofaccount;
	public static boolean investmentComponentsClientRelationship() throws IOException
	{
		try 
		{
			XLUtilsV2 xl=new XLUtilsV2();
			String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			Logout no=new Logout();
			no.mLogin();
			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
			d.findElement(By.linkText("Sponsors")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText(sponsor)).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Sleeper.sleepTightInSeconds(2);
			
			
			d.findElement(By.linkText("Fees")).click();
			List<WebElement> feenames=d.findElements(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div[3]/div/div/div/h2/a"));
			int feenamessize=feenames.size();
			if (feenamessize==1) 
			{ 
				d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Add Fee Schedule']")).click();
		        Sleeper.sleepTightInSeconds(2);
		        Select feelist=new Select(d.findElement(By.xpath("//*[contains(@class,'form-control')]")));
		        feelist.selectByIndex(2);
		        d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Save Fee Schedules']")).click();
		        Sleeper.sleepTightInSeconds(2);
		        d.navigate().refresh();
		        Sleeper.sleepTightInSeconds(5);
				
			} else if (feenamessize==0) 
			{
				d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Add Fee Schedule']")).click();
		        Sleeper.sleepTightInSeconds(2);
		        Select feelist=new Select(d.findElement(By.xpath("//*[contains(@class,'form-control')]")));
		        feelist.deselectByIndex(1);
		        d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Save Fee Schedules']")).click();
		        Sleeper.sleepTightInSeconds(2);
		        d.navigate().refresh();
		        Sleeper.sleepTightInSeconds(5);
		        d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Add Fee Schedule']")).click();
		        Sleeper.sleepTightInSeconds(2);
		       // Select feelist=new Select(d.findElement(By.xpath("//*[contains(@class,'form-control')]")));
		        feelist.selectByIndex(2);
		        d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Save Fee Schedules']")).click();
		        Sleeper.sleepTightInSeconds(2);
		        d.navigate().refresh();
		        Sleeper.sleepTightInSeconds(5);
				
			}
			
			d.findElement(By.linkText("Investment Components")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Sleeper.sleepTightInSeconds(2);
			WebElement invcom=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/ul[1]/li/div/label/input"));
			if (!invcom.isSelected()) {
				invcom.click();
			}
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/button")).click();
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.linkText("Integration")).click();
			Sleeper.sleepTightInSeconds(2);
			// String userurl=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div[1]/div[2]/div[1]/p/a")).getText();
			// Sleeper.sleepTightInSeconds(2);
			 //d.get(userurl);
			 d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div[1]/div[2]/div[1]/p/a")).click();
			 Sleeper.sleepTightInSeconds(2);
			 java.util.Set<String> winna=d.getWindowHandles();
			
			 Object[] windows=winna.toArray();
			 String win1=windows[0].toString();
			 String win2=windows[1].toString();
			 d.switchTo().window(win2);
			 
			 d.findElement(By.id("firstName")).sendKeys(fname);			  
			  d.findElement(By.id("lastName")).sendKeys(lname);			 
			  d.findElement(By.id("emailAddress")).sendKeys(email);			  
			  d.findElement(By.id("password")).sendKeys(pass);
			  d.findElement(By.id("confirmPassword")).sendKeys(cpass);
			  d.findElement(By.id("nextButton")).click();
			  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			  Sleeper.sleepTightInSeconds(5);			  
			  d.findElement(By.id("dateOfBirth")).sendKeys(DOB);				 
			  d.findElement(By.id("SSN")).sendKeys(SSN);			 
			  Select ms=new Select(d.findElement(By.id("maritalStatus")));
			  ms.selectByVisibleText(maritalstatus);			  
			  d.findElement(By.id("phone")).sendKeys(phone);			  
			  d.findElement(By.id("street")).sendKeys(address1);			  
			  d.findElement(By.id("aptSte")).sendKeys(address2);			  
			  d.findElement(By.id("city")).sendKeys(city);			  
			  Select st=new Select(d.findElement(By.id("state")));
			  st.selectByVisibleText(state);			  
			  d.findElement(By.id("zip")).sendKeys(zipcode);
			  Sleeper.sleepTightInSeconds(2);			  
			  Select toa=new Select(d.findElement(By.id("accountType")));
			  toa.selectByVisibleText(typeofaccount);
			  Sleeper.sleepTightInSeconds(2);
			  
			  d.findElement(By.xpath("html/body/div[3]/form/div[6]/div/div[2]/div/div[1]/div/table/tbody/tr[1]/td/h5/label/span")).click();
			  Sleeper.sleepTightInSeconds(2);
			  JavascriptExecutor js = (JavascriptExecutor)d;
		        js.executeScript("window.scrollBy(0,+3200)", "");
			
			  d.findElement(By.xpath("html/body/div[3]/form/div[8]/div[2]/div[2]/label/span")).click();
			  d.findElement(By.id("nextButton")).click();
			  Sleeper.sleepTightInSeconds(3);
			  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			 
			  
			  if (d.findElement(By.xpath("//span[contains(., 'Interested Party')]")).isDisplayed())
			{
				d.switchTo().window(win2).close();
				d.switchTo().window(win1);
				return true;			
			} 
			else 
			{
				d.switchTo().window(win2).close();
				d.switchTo().window(win1);
				return false;
			}		
		} 
		catch (Exception e) 
		{
			d.get(url);
			System.out.println(e);
			return false;
		}
	}
}