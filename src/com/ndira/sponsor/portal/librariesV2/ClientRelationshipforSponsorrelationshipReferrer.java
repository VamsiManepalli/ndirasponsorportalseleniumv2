package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class ClientRelationshipforSponsorrelationshipReferrer extends NDiraConstantsV2
{	
	public static boolean clientRelationshipforSponsorrelationshipReferrer() throws IOException
	{
		try 
		{
			XLUtilsV2 xl=new XLUtilsV2();
			String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			Logout no=new Logout();
			no.mLogin();
			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
			d.findElement(By.linkText("Sponsors")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText(sponsor)).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			JavascriptExecutor js = (JavascriptExecutor)d;
	        js.executeScript("window.scrollBy(0,+1800)", "");
			//WebElement weadvisor1=d.findElement(By.xpath("//label[@class='ng-binding' and contains(text(), 'Advisor')]"));
			//System.out.println(weadvisor1.getText());
	        WebElement weadvisor=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[3]/label/input"));
			if (!weadvisor.isSelected()) {
				weadvisor.click();
				Sleeper.sleepTightInSeconds(5);

			} 
				 Select clientre=new Select(d.findElement(By.id("client-relationship")));
				  //Select select = new Select(d.findElement(By.xpath(".//*[@id='admin-new-sponsor-details-overview']/div/div[1]/div[1]/div[2]/div[5]/select")));
					WebElement option = clientre.getFirstSelectedOption();
					String exp = option.getText();
			//d.findElement(By.xpath("//label[@class='ng-binding' and contains(text(), 'Advisor')]")).click();
			if (exp.equalsIgnoreCase(""))
			{
				
				return true;			
			} 
			else 
			{
				
				return false;
			}		
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}

