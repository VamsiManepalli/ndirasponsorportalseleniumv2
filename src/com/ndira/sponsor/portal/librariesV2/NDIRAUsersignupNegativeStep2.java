package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class NDIRAUsersignupNegativeStep2 extends NDIRAUsersignupNegativeStep1
{
	public static String fname,lname,email,pass,cpass,DOB,SSN,maritalstatus,phone,address1,address2,city,state,zipcode,typeofaccount;
	public static boolean NDIRAUsersignupNegativeStep2() throws IOException
	{
		try 
		{
		  XLUtilsV2 xl=new XLUtilsV2();
		  String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
		  String tcsheet="TestCases";
		  String tssheet="TestSteps";
		  
		  
		  NDIRAUsersignupNegativeStep1 nds1=new NDIRAUsersignupNegativeStep1();
		  nds1.NDIRAUsersignupNegative();
		  
		  Sleeper.sleepTightInSeconds(2);
		  
		  d.findElement(By.id("dateOfBirth")).sendKeys(DOB);
		  
		  d.findElement(By.id("SSN")).sendKeys(SSN);
		
		  Select ms=new Select(d.findElement(By.id("maritalStatus")));
		  ms.selectByVisibleText(maritalstatus);
		  
		  d.findElement(By.id("phone")).sendKeys(phone);
		  
		  d.findElement(By.id("street")).sendKeys(address1);
		  
		  d.findElement(By.id("aptSte")).sendKeys(address2);
		 
		  d.findElement(By.id("city")).sendKeys(city);
		 
		  Select st=new Select(d.findElement(By.id("state")));
		  st.selectByVisibleText(state);
		  
		  d.findElement(By.id("zip")).sendKeys(zipcode);
		  Sleeper.sleepTightInSeconds(2);
		 
		  Select toa=new Select(d.findElement(By.id("accountType")));
		  toa.selectByVisibleText(typeofaccount);
		  d.findElement(By.xpath("html/body/div[3]/form/div[7]/div[2]/div[2]/label/span")).click();
		  Sleeper.sleepTightInSeconds(2);
		  d.findElement(By.id("nextButton")).click();
		  
		  String expurl;
		  expurl=d.getCurrentUrl();
		  if (expurl.contains(expurl))
		  {
			  return true;	
		  }
		  else
		  {
			return false;
		  }
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
