package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class AddInvestmentNegative extends NDiraConstantsV2
{
	public static String Investment,Type,Description;
	public static boolean AddInvestmentNegative() throws IOException
	{
		try 
		{
			Sponsorlogout no=new Sponsorlogout();
		  	no.mLogin();
		  	d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			d.findElement(By.linkText("Add Investment")).click();
			d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/div/form/div[1]/div[1]/div/input")).sendKeys(Investment);
			Select invtype=new Select(d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/div/form/div[1]/div[2]/div/select")));
			invtype.selectByVisibleText(Type);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/div/form/div[1]/div[3]/div/textarea")).sendKeys(Description);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/div/form/ul/li[2]/button")).click();
			Sleeper.sleepTightInSeconds(2);
			String expmsg;
			expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/div/p[2]")).getText();
			if (expmsg.equalsIgnoreCase("There was an error with your investment. Please review and resubmit."))
			{
				return true;
			} 
			else 
			{
				return false;
			}
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
