package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class MarketingMaterials extends NDiraConstantsV2
{
	public static String Title,Desc;
	public static boolean MarketingMaterials() throws IOException
	{
		try 
		{
			XLUtilsV2 xl=new XLUtilsV2();
			String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			
			Logout no=new Logout();
		  	no.mLogin();
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		d.findElement(By.linkText("Sponsors")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.linkText(sponsor)).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.linkText("Marketing Materials")).click();
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[2]/div[1]")).click();
		Sleeper.sleepTightInSeconds(5);
		String windows_component = NdiraUtilsV2.getProperty("file.windows.component");
		Runtime.getRuntime().exec(windows_component);
		
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div/form/div[1]/input")).sendKeys(Title);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div/form/div[2]/textarea")).sendKeys(Desc);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div/form/ul/li[1]/button")).click();
		String expmsg, acmsg;		
		acmsg="updated.";
		Sleeper.sleepTightInSeconds(3);
		expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div/div/div/div")).getText();
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div[1]/form/ul/li[2]/button")).click();
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/button[2]")).click();
		if (expmsg.contains(acmsg)) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
		
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}

}
