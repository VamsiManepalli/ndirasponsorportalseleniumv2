package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;


public class ManageTeamNegative extends NDiraConstantsV2
{
	public static String fname,lname,email;
	public static boolean ManageTeamNegative() throws IOException
	{
		try 
		{
			Sponsorlogout no=new Sponsorlogout();
		  	no.mLogin();
		  	d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  	d.findElement(By.linkText("Manage Team")).click();
		  	d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  	d.findElement(By.id("new-first-name")).sendKeys(fname);
		  	d.findElement(By.id("new-last-name")).sendKeys(lname);
		  	d.findElement(By.id("new-email")).sendKeys(email);
		  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div/div/div[1]/div/form/div[4]/div/button")).click();
		  	Sleeper.sleepTightInSeconds(1);
		  	String expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div/div/div[1]/div/p")).getText();
		  	if (SponsorTeamMemberList.getAddTeamMemberMessages().contains(expmsg))
		  	{
		  		return true;
			} 
		  	else if(expmsg.isEmpty())
		  	{
				throw new ElementNotVisibleException("Element with empty message found.");
			}
		  	else
		  	{
		  		return false;
			}
		}
		catch (Exception e) 
		{	
			
			Sleeper.sleepTightInSeconds(5);
			d.get(url);
			
			System.out.println(e);
			return false;
		}
	}
}
