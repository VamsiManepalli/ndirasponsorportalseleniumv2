package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;

public class Sitedata_FeeSchedule_Update extends NDiraConstantsV2
{
	public static String aname,adisplayname,afeeschedulename,aoraclecode,aDisplaytext;
	public static boolean Sitedata_FeeSchedule_UpdateTest(String name,String displayname,String oraclecode,String feeschedulename,String Displaytext) throws IOException {
		try 
		{
		Logout no=new Logout();
	  	no.mLogin();
	  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.linkText("Site Data")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.linkText("Fee Schedules")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		WebElement table;
		table=d.findElement(By.xpath("//*[@id='admin-new-fee-schedules-overview' ]"));
		List<WebElement> rows,cols;
		rows=table.findElements(By.tagName("tr"));
		int rc=rows.size();
		cols=rows.get(rc-1).findElements(By.xpath("//a[@ng-click='editItem(this.feeSchedule)']"));
		int cc=cols.size();
	    cols.get(cc-1).click();
	    JavascriptExecutor js = (JavascriptExecutor)d;
        js.executeScript("window.scrollBy(0,-3600)", "");
        d.findElement(By.xpath("//input[@type='text' and @placeholder='name'] ")).clear();
	    d.findElement(By.xpath("//input[@type='text' and @placeholder='name'] ")).sendKeys(name);
	    d.findElement(By.xpath("//input[@type='text' and @placeholder='display name'] ")).clear();
		d.findElement(By.xpath("//input[@type='text' and @placeholder='display name'] ")).sendKeys(displayname);
		d.findElement(By.xpath("//input[@type='text' and @placeholder='fee schedule name'] ")).clear();
		d.findElement(By.xpath("//input[@type='text' and @placeholder='fee schedule name'] ")).sendKeys(feeschedulename);
		d.findElement(By.xpath("//input[@type='number'] ")).clear();
		d.findElement(By.xpath("//input[@type='number'] ")).sendKeys(oraclecode);
		d.findElement(By.xpath("//textarea[@type='text' and @placeholder='display text'] ")).clear();
		d.findElement(By.xpath("//textarea[@type='text' and @placeholder='display text'] ")).sendKeys(Displaytext);
		d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Save'] ")).click();
		
		if (d.findElement(By.xpath("//*[span='Fee Schedule updated.']")).isDisplayed()) 
		{
			return true;
			
		} else
		{
			return false;

		}		
		}
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
	    
	}
		
}
