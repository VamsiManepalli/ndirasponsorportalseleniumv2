package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class FeeScheduleNAmeDisplayinginOpenAccount extends NDiraConstantsV2
{
	public static String feenamesinopenaccoun,feenameslink,asponsorname,fname,lname,email,pass,cpass,DOB,SSN,maritalstatus,phone,address1,address2,city,state,zipcode,typeofaccount,feeshedule1,feeschedule2;
	public static boolean feeScheduleNameDisplayinginOpenAccount(String sponsorname) throws IOException
	{
		try 
		{
		    Logout no=new Logout();
		  	no.mLogin();
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div/div[2]/div/div[2]/input")).sendKeys(sponsorname);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div/div[2]/div/div[2]/input")).sendKeys(Keys.ENTER);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.linkText(sponsorname)).click();
			Sleeper.sleepTightInSeconds(2);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText("Fees")).click();
			List<WebElement> feenames=d.findElements(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div[3]/div/div/div/h2/a"));
			int feenamessize=feenames.size();
			System.out.println(feenamessize);
			
			if (feenamessize==1) 
			{ 
				d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Add Fee Schedule']")).click();
		        Sleeper.sleepTightInSeconds(2);
		        Select feelist=new Select(d.findElement(By.xpath("//*[contains(@class,'form-control')]")));
		        feelist.selectByIndex(2);
		        d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Save Fee Schedules']")).click();
		        Sleeper.sleepTightInSeconds(2);
		        d.navigate().refresh();
		        Sleeper.sleepTightInSeconds(5);
				
			} else if (feenamessize==0) 
			{
				d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Add Fee Schedule']")).click();
		        Sleeper.sleepTightInSeconds(2);
		        Select feelist=new Select(d.findElement(By.xpath("//*[contains(@class,'form-control')]")));
		        feelist.deselectByIndex(1);
		        d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Save Fee Schedules']")).click();
		        Sleeper.sleepTightInSeconds(2);
		        d.navigate().refresh();
		        Sleeper.sleepTightInSeconds(5);
		        d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Add Fee Schedule']")).click();
		        Sleeper.sleepTightInSeconds(2);
		       // Select feelist=new Select(d.findElement(By.xpath("//*[contains(@class,'form-control')]")));
		        feelist.selectByIndex(2);
		        d.findElement(By.xpath("//*[contains(@class,'btn') and text()='Save Fee Schedules']")).click();
		        Sleeper.sleepTightInSeconds(2);
		        d.navigate().refresh();
		        Sleeper.sleepTightInSeconds(5);
				
			}
			List<WebElement> feenamesafteadding=d.findElements(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div[3]/div/div/div/h2/a"));
			int feenamessizeafteradding=feenamesafteadding.size();
			for (int i = 0; i < feenamessizeafteradding; i++) 
			{
				 feenameslink=feenamesafteadding.get(i).getText();
			}
			
			
			d.findElement(By.linkText("Investment Components")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Sleeper.sleepTightInSeconds(2);
			
			
			WebElement invcom=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/ul[1]/li/div/label/input"));
			if (!invcom.isSelected()) {
				invcom.click();
			}
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/button")).click();
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.linkText("Integration")).click();
			Sleeper.sleepTightInSeconds(2);
			
			 d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div[1]/div[2]/div[1]/p/a")).click();
			 Sleeper.sleepTightInSeconds(2);
			 java.util.Set<String> winna=d.getWindowHandles();
			
			 Object[] windows=winna.toArray();
			 String win1=windows[0].toString();
			 String win2=windows[1].toString();
			 d.switchTo().window(win2);
			
			 d.findElement(By.id("firstName")).sendKeys(fname);
			  
			  d.findElement(By.id("lastName")).sendKeys(lname);
			 
			  d.findElement(By.id("emailAddress")).sendKeys(email);
			  
			  d.findElement(By.id("password")).sendKeys(pass);
			  
			  d.findElement(By.id("confirmPassword")).sendKeys(cpass);
			  d.findElement(By.id("nextButton")).click();
			  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			  Sleeper.sleepTightInSeconds(5);
			  
			  d.findElement(By.id("dateOfBirth")).sendKeys(DOB);
				 
			  d.findElement(By.id("SSN")).sendKeys(SSN);
			 
			  Select ms=new Select(d.findElement(By.id("maritalStatus")));
			  ms.selectByVisibleText(maritalstatus);
			  
			  d.findElement(By.id("phone")).sendKeys(phone);
			  
			  d.findElement(By.id("street")).sendKeys(address1);
			  
			  d.findElement(By.id("aptSte")).sendKeys(address2);
			  
			  d.findElement(By.id("city")).sendKeys(city);
			  
			  Select st=new Select(d.findElement(By.id("state")));
			  st.selectByVisibleText(state);
			  
			  d.findElement(By.id("zip")).sendKeys(zipcode);
			  Sleeper.sleepTightInSeconds(2);
			  
			  Select toa=new Select(d.findElement(By.id("accountType")));
			  toa.selectByVisibleText(typeofaccount);
			  Sleeper.sleepTightInSeconds(2);
			  
			  d.findElement(By.xpath("html/body/div[3]/form/div[6]/div/div[2]/div/div[1]/div/table/tbody/tr[1]/td/h5/label/span")).click();
			  Sleeper.sleepTightInSeconds(2);
			  JavascriptExecutor js = (JavascriptExecutor)d;
		        js.executeScript("window.scrollBy(0,+3200)", "");
			 
			  List<WebElement> feenamesinopenaccount=d.findElements(By.xpath("html/body/div[3]/form/div[6]/div[2]/div[2]/div/div/div/table/tbody/tr[1]/td/h5/label"));
			  int feenamesinopenaccountcount=feenamesinopenaccount.size();
			
			  for (int i = 0; i <feenamesinopenaccountcount; i++) 
			  {
				   feenamesinopenaccoun=feenamesinopenaccount.get(i).getText();
				
			  }
			  String exptext,acttext;
			  exptext=feenameslink;
			  acttext=feenamesinopenaccoun;
			  System.out.println(exptext);
			  System.out.println(acttext);
			  
			if (exptext.contains(acttext))
			{
				d.switchTo().window(win2).close();
				d.switchTo().window(win1);
				return true;			
			} 
			else 
			{
				d.switchTo().window(win2).close();
				d.switchTo().window(win1);
				return false;
			}		
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}

