package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class SponsorUsersignupNegativeStep1 extends NDiraConstantsV2
{
	public static String fname,lname,email,pass,cpass;
	public static boolean SponsorUsersignupNegativeStep1() throws IOException 
	{
		try 
		{			
			Sponsorlogout no=new Sponsorlogout();
		  	no.mLogin();
		  	Sleeper.sleepTightInSeconds(3);
		  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[1]/ul/li[5]/ul/li[1]/a")).click();
		  	Sleeper.sleepTightInSeconds(2);
		  	
		  	String link=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div[2]/div/p[2]/a")).getText();
		  	Sleeper.sleepTightInSeconds(3);
		  	d.get(link);
		  	Sleeper.sleepTightInSeconds(2);
		  	
		  	d.findElement(By.id("firstName")).sendKeys(fname);
		  	  
		    d.findElement(By.id("lastName")).sendKeys(lname);
		  	  
		    d.findElement(By.id("emailAddress")).sendKeys(email);
		  	  
		    d.findElement(By.id("password")).sendKeys(pass);
		  	 
		    d.findElement(By.id("confirmPassword")).sendKeys(cpass);
		  	  
		    d.findElement(By.id("nextButton")).click();
		    Sleeper.sleepTightInSeconds(2);
		    
			String expurl;
			expurl=d.getCurrentUrl();
			if (expurl.contains("https://app.ndiraplatform-uat.com/admin-new/sponsor/5b2a4d383152897be33a1bf9/integration"))
			{
				return false;	
			}
			else
			{
				return true;
			}
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
	
}
