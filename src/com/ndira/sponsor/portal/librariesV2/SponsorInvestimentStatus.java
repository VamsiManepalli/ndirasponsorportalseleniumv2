package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class SponsorInvestimentStatus extends NDiraConstantsV2
{
	public static String invstatus, filestatus;
	static XLUtilsV2 xl = new XLUtilsV2();
	static String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
	String tcsheet = "TestCases";
	static String tssheet = "TestSteps";

	public static boolean sponsorInvestmentStatus() throws IOException 
	{
		try 
		{
			SponsorAddInvestiment si=new SponsorAddInvestiment();
			si.SponsorAddInvestment();
			
			Sponsorlogout spl=new Sponsorlogout();
			spl.Logout();
	
			Logout no = new Logout();
			no.mLogin();
			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	
			d.findElement(By.linkText("Sponsors")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String sponsor = xl.getCellData(xlfile, tssheet, 1, 5);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input"))
					.sendKeys(sponsor);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input"))
					.sendKeys(Keys.ENTER);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText(sponsor)).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText("Investments")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By
					.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div[3]/div[2]/table/tbody/tr[1]/td[4]/a"))
					.click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	
			Select inv = new Select(d.findElement(By.xpath(
					"html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/div/div[2]/form/div[5]/select")));
			inv.selectByVisibleText(invstatus);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	
			Select file = new Select(d.findElement(By.xpath(
					"html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div[2]/div[2]/div/div[2]/table/tbody/tr/td[4]/select")));
			file.selectByVisibleText(filestatus);
			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/div/div[2]/form/ul/li[1]/button")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			
			Logout m = new Logout();
			m.Logout();
	
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Sponsorlogout sl = new Sponsorlogout();
			sl.mLogin();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[1]/ul/li[4]/ul/li[2]/a")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	
			String invstat = d
					.findElement(By
							.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/div[1]/div/div/form/table/tbody/tr[4]/td/p[1]"))
					.getText();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String filestat = d
					.findElement(By
							.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/div/div/div/form/div[3]/table/tbody/tr/td[4]"))
					.getText();
			if (invstat.contains(invstatus))
			{
				return true;
			} 
			else 
			{
				return false;
			}
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
		
	}

	public static boolean sponsorInvestmentDocumentStatus() throws IOException 
	{
		try 
		{
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String filestat = d
					.findElement(By
							.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/div/div/div/form/div[3]/table/tbody/tr/td[4]"))
					.getText();
			if (filestat.contains(filestatus)) 
			{
				return true;
			} 
			else 
			{
				return false;
			}
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
