package com.ndira.sponsor.portal.librariesV2;


import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;
import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;

public class CompanyOverview extends NDiraConstantsV2
{
	public static String sponsor,sponsorrelationship,picategory,type,Howdidyouhear,OverrideFeeBuffer,expiry,min,max,amount;
    public static boolean companyOverview() throws IOException
    {
	  try
		{
		  Logout no=new Logout();
		  no.mLogin();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
			d.findElement(By.linkText("Sponsors")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText(sponsor)).click();			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  		
			//To select Advisor
			if (sponsorrelationship.equalsIgnoreCase("Advisor")) 
			{			
			WebElement referrer=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[3]/label/input"));
	        WebElement advisor=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[1]/label/input"));
	        WebElement provider=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[2]/label/input"));
	        if (referrer.isSelected()) {
				referrer.click();
             } 
	        if (!advisor.isSelected()) {
				advisor.click();
             } 
	        if (provider.isSelected()) {
				provider.click();
             } 
	        
			}
			
			//To select Provider
			if (sponsorrelationship.equalsIgnoreCase("Provider")) 
			{			
			WebElement referrer=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[3]/label/input"));
	        WebElement advisor=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[1]/label/input"));
	        WebElement provider=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[2]/label/input"));
	        if (referrer.isSelected()) {
				referrer.click();
             } 
	        if (advisor.isSelected()) {
				advisor.click();
             } 
	        if (!provider.isSelected()) {
				provider.click();
             } 
	        
			}
			
			//To select Referrer
			if (sponsorrelationship.equalsIgnoreCase("Referrer")) 
			{			
			WebElement referrer=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[3]/label/input"));
	        WebElement advisor=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[1]/label/input"));
	        WebElement provider=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[2]/label/input"));
	        if (!referrer.isSelected()) {
				referrer.click();
             } 
	        if (advisor.isSelected()) {
				advisor.click();
             } 
	        if (provider.isSelected()) {
				provider.click();
             } 
	        
			}
			
			//To select Advisor/Provider
			if (sponsorrelationship.equalsIgnoreCase("Advisor/Provider")) 
			{			
			WebElement referrer=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[3]/label/input"));
	        WebElement advisor=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[1]/label/input"));
	        WebElement provider=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[2]/label/input"));
	        if (referrer.isSelected()) {
				referrer.click();
             } 
	        if (!advisor.isSelected()) {
				advisor.click();
             } 
	        if (!provider.isSelected()) {
				provider.click();
             } 
	        
			}
			
			Select pic=new Select(d.findElement(By.id("primary-investment-category")));
			pic.selectByVisibleText(picategory);
			
			Select st=new Select(d.findElement(By.xpath("//select[contains(@ng-model, 'sponsorType')]")));
			st.selectByVisibleText(type);
			
			Select sr=new Select(d.findElement(By.xpath("//select[contains(@ng-model, 'sponsorHowDidYouHear')]")));
			sr.selectByVisibleText(Howdidyouhear);
			
			List<WebElement> save=d.findElements(By.xpath("//button[@class='btn btn-primary' and @ng-click='updateSponsor()']"));
			save.get(0).click();
			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
			Sleeper.sleepTightInSeconds(3);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[2]/div[2]/div[1]")).click();
			Sleeper.sleepTightInSeconds(3);
		
			String windows_component = NdiraUtilsV2.getProperty("file.windows.component");
			Runtime.getRuntime().exec(windows_component);
			JavascriptExecutor js = (JavascriptExecutor)d;
	        js.executeScript("window.scrollBy(0,-600)", "");
		
	        d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
		
	        
	        WebElement onlineapplication,Clients,Investments,Integrations;			
	        onlineapplication=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/ul/li[1]/div/label/input"));
	        System.out.println("hi");
	        Clients=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/ul/li[2]/div/label/input"));
	        Investments=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/ul/li[3]/div/label/input"));
	        Integrations=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/ul/li[4]/div/label/input"));
	        
	        if (!onlineapplication.isSelected())
	        {
	        	onlineapplication.click();
			}

	        if (!Clients.isSelected())
	        {
	        	Clients.click();
			}

	        if (!Investments.isSelected())
	        {
	        	Investments.click();
			}

	        if (!Integrations.isSelected())
	        {
	        	Integrations.click();
			}        
	    
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		d.findElement(By.id("allocation-expiration")).clear();		
		d.findElement(By.id("allocation-expiration")).sendKeys(expiry);		

		d.findElement(By.id("allocation-minimum")).clear();		
		d.findElement(By.id("allocation-minimum")).sendKeys(min);		
			
		d.findElement(By.id("allocation-maximum")).clear();		
		d.findElement(By.id("allocation-maximum")).sendKeys(max);		
		if (OverrideFeeBuffer.equalsIgnoreCase("Yes"))
		{
			WebElement override=d.findElement(By.id("override-fee-override"));
			if (!override.isSelected())
			{
				override.click();
			}
			Sleeper.sleepTightInSeconds(5);
			d.findElement(By.id("override-fee-amount")).clear();
			d.findElement(By.id("override-fee-amount")).sendKeys(amount);
		}
		save.get(1).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
		
		String expmsg,actmsg;
		expmsg="Sponsor successfully updated.";
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		JavascriptExecutor jse = (JavascriptExecutor)d;
        jse.executeScript("window.scrollBy(0,-250)", "");
		actmsg=d.findElement(By.xpath("//span[@class='ng-binding ng-scope']")).getText();		
		if (actmsg.contains(expmsg)) 
		{
			return true;
		} 
		else
		{
			return false;
		}

		} 
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
		
  }
}
