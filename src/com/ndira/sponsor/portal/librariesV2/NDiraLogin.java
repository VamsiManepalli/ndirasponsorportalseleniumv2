package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class NDiraLogin extends NDiraConstantsV2 
{
	
	public static String suname, spwd;
	public boolean Login() throws IOException 
	{
		try
		{			
			d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			d.manage().window().maximize();			
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[2]/input")).sendKeys(suname);
			Sleeper.sleepTightInSeconds(5);
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[3]/input")).sendKeys(spwd);
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[4]/div[2]/button")).click();
			d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[7]/a")).click();
			d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Sleeper.sleepTightInSeconds(2);
			String expurl, acturl;
			expurl = url;
			acturl = d.getCurrentUrl();
			if (acturl.contains(expurl))
			{			
				return true;
			} 
			else
			{				
				return false;
			}
		} 
		catch (Exception e)
		{
			
			d.get(url);
			System.out.println(e);
			return false;
		}

	}
}
