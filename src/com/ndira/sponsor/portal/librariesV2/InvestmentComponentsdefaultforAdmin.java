
package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class InvestmentComponentsdefaultforAdmin extends NDiraConstantsV2
{	
	public static boolean investmentComponentsdefaultforAdmin() throws IOException
	{
		try 
		{
			XLUtilsV2 xl=new XLUtilsV2();
			String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			Logout no=new Logout();
			no.mLogin();
			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
			d.findElement(By.linkText("Sponsors")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText(sponsor)).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			JavascriptExecutor js = (JavascriptExecutor)d;
	        js.executeScript("window.scrollBy(0,+1800)", "");
	        
	        WebElement wereferrer=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[3]/label/input"));
	        WebElement weadvisor1=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[1]/label/input"));
	        WebElement weprovider=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/sponsorrelationship/div/ul/li[2]/label/input"));
	        if (wereferrer.isSelected()) {
				wereferrer.click();
				Sleeper.sleepTightInSeconds(5);
             } 
	        if (!weadvisor1.isSelected()) {
				weadvisor1.click();
				Sleeper.sleepTightInSeconds(5);
             } 
	        if (weprovider.isSelected()) {
				weprovider.click();
				Sleeper.sleepTightInSeconds(5);
             } 
	        d.findElement(By.linkText("Investment Components")).click();
	        Sleeper.sleepTightInSeconds(2);
				
			if (d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/ul[1]/li/div/label/input")).isSelected())
			{
				return true;			
			} 
			else 
			{
				return false;
			}		
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}

