package com.ndira.sponsor.portal.librariesV2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class InvalidLogin extends com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2
{
	public static String uname,pass;
	public static boolean InvalidLogin()
	{
		try 
		{
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[2]/input")).sendKeys(uname);
		  d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[3]/input")).sendKeys(pass);
		  d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[4]/div[2]/button")).click();
		  String expmsg;
		  Sleeper.sleepTightInSeconds(2);
		  expmsg=d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[1]/div")).getText();
		  if (LoginMessagesList.getLoginMessagesList().contains(expmsg)) 
		  {
			return true;
		  } 
		  else
		  {
			  return false;
		  }	
		}
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
