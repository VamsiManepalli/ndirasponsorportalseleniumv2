package com.ndira.sponsor.portal.librariesV2;

import java.util.ArrayList;
import java.util.List;

public class LoginMessagesList 
{
	public static List<String> getLoginMessagesList()
	{
		List<String> list = new ArrayList<>();
		list.add("must provide an email and password");
		list.add("Incorrect username or password.");
		
		return list;	
	}
}
