package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class Sponsorlogout extends NDiraConstantsV2
{
	public static void Logout() 
	{
		try 
		{
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.linkText("Log out")).click();	
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
    }
	public static void mLogin() throws IOException
	  {
		try 
		{
			XLUtilsV2 xl=new XLUtilsV2();
			String xlfile = NdiraUtilsV2.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			  
			String sname =xl.getCellData(xlfile, tssheet, 1, 9);
			String spass =xl.getCellData(xlfile, tssheet, 1, 13);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[2]/input")).clear();
			
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[2]/input")).sendKeys(sname);
			
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[3]/input")).clear();
			
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[3]/input")).sendKeys(spass);
			
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[4]/div[2]/button")).click();
		}
		catch (Exception e) 
		{
			System.out.println(e);
		} 
	  }
	
}
