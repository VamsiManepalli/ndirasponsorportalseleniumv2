package com.ndira.sponsor.portal.librariesV2;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.jasper.tagplugins.jstl.core.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utilsV2.NDiraConstantsV2;
import com.ndira.sponsor.portal.utilsV2.NdiraUtilsV2;
import com.ndira.sponsor.portal.utilsV2.XLUtilsV2;

public class DepositoryInformationInStep3Validation extends NDiraConstantsV2
{
	public static String names,asponsorname,fname,lname,email,pass,cpass,DOB,SSN,maritalstatus,phone,address1,address2,city,state,zipcode,typeofaccount;
	public static boolean depositoryInformationInStep3Validation(String sponsorname) throws IOException
	{
		try 
		{
		    Logout no=new Logout();
		  	no.mLogin();
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div/div[2]/div/div[2]/input")).sendKeys(sponsorname);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div/div[2]/div/div[2]/input")).sendKeys(Keys.ENTER);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.linkText(sponsorname)).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText("Investment Components")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Sleeper.sleepTightInSeconds(2);			
			WebElement invcom=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/ul[4]/li/div/label/input"));
			if (!invcom.isSelected()) 
			{
				invcom.click();
			}
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div[2]/button")).click();
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.linkText("Depositories")).click();
		       d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/div[2]/div[1]/div[9]/h3/span/i[1]")).click();
		       d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/div[2]/div[1]/div[9]/div/div/div/ul/li[2]/div/label/input")).click();
		       WebElement depositoryname=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/div[2]/div[1]/div[9]/div/div/div/ul/li[2]/div/label/input"));
		      
		       if (!depositoryname.isSelected())
		       {
				depositoryname.click();
			   }
		      // String depname=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/div[2]/div[2]/div[1]/div/label")).getText();
		       //System.out.println(depname);
		       List<WebElement> li=d.findElements(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/div[2]/div[2]/div/div/label"));
		        System.out.println(li.size());
		        names=li.get(0).getText();
				System.out.println(names);
				Sleeper.sleepTightInSeconds(2);
			
			d.findElement(By.linkText("Integration")).click();
			 Sleeper.sleepTightInSeconds(2);
			
			 d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div[1]/div[2]/div[1]/p/a")).click();
			 Sleeper.sleepTightInSeconds(2);
			 d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			 java.util.Set<String> winna=d.getWindowHandles();
			 Object[] windows=winna.toArray();
			 String win1=windows[0].toString();
			 String win2=windows[1].toString();
			 d.switchTo().window(win2);
			 
			 //open account 1st step
			 d.findElement(By.id("firstName")).sendKeys(fname);			  
			  d.findElement(By.id("lastName")).sendKeys(lname);			 
			  d.findElement(By.id("emailAddress")).sendKeys(email);			  
			  d.findElement(By.id("password")).sendKeys(pass);			  
			  d.findElement(By.id("confirmPassword")).sendKeys(cpass);
			  d.findElement(By.id("nextButton")).click();
			  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			  Sleeper.sleepTightInSeconds(2);
			  
			  //open account second step			  
			  d.findElement(By.id("dateOfBirth")).sendKeys(DOB);				 
			  d.findElement(By.id("SSN")).sendKeys(SSN);			 
			  Select ms=new Select(d.findElement(By.id("maritalStatus")));
			  ms.selectByVisibleText(maritalstatus);			  
			  d.findElement(By.id("phone")).sendKeys(phone);			  
			  d.findElement(By.id("street")).sendKeys(address1);			  
			  d.findElement(By.id("aptSte")).sendKeys(address2);			  
			  d.findElement(By.id("city")).sendKeys(city);			  
			  Select st=new Select(d.findElement(By.id("state")));
			  st.selectByVisibleText(state);			  
			  d.findElement(By.id("zip")).sendKeys(zipcode);
			  Sleeper.sleepTightInSeconds(2);			  
			  Select toa=new Select(d.findElement(By.id("accountType")));
			  toa.selectByVisibleText(typeofaccount);
			  Sleeper.sleepTightInSeconds(2);			  
			  d.findElement(By.xpath("html/body/div[3]/form/div[6]/div/div[2]/div/div[1]/div/table/tbody/tr[1]/td/h5/label/span")).click();
			  Sleeper.sleepTightInSeconds(2);
			  JavascriptExecutor js = (JavascriptExecutor)d;
		        js.executeScript("window.scrollBy(0,+3200)", "");			
			  d.findElement(By.xpath("html/body/div[3]/form/div[8]/div[2]/div[2]/label/span")).click();
			  d.findElement(By.id("nextButton")).click();
			  Sleeper.sleepTightInSeconds(3);
			  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			 
			 js.executeScript("window.scrollBy(0,+1200)", "");
			 String depinftext;
			 Select deplist=new Select(d.findElement(By.id("depositoryChoice")));
			deplist.selectByIndex(1);
			depinftext=deplist.getOptions().get(1).getText();
			Select depbranchlist=new Select(d.findElement(By.id("depositoryType")));
			depbranchlist.selectByIndex(1);
			String depbranchtext=depbranchlist.getOptions().get(1).getText();
			System.out.println(depbranchtext);
			if (depbranchtext.contains(names))
			{				
				d.switchTo().window(win2).close();
				d.switchTo().window(win1);
				return true;
				
			} 
			else 
			{
				d.switchTo().window(win2).close();
				d.switchTo().window(win1);
				
				return false;
			}		
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
		
	}
}
