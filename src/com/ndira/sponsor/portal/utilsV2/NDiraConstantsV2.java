package com.ndira.sponsor.portal.utilsV2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

	public class NDiraConstantsV2
	{
		 //public static String path="C:\\Users\\ADMIN\\Downloads\\chromedriver.exe";		
		 //public static String c=System.setProperty("webdriver.chrome.driver", path);
		//public static WebDriver d=new ChromeDriver();
	   
		public static WebDriver d=new FirefoxDriver();
	    public static String url=NdiraUtilsV2.getProperty("url");
	  
		
		//public static WebDriver d = new SafariDriver();	
		//public static String url=NdiraUtils.getProperty("url");
		
	  @BeforeTest
	  public void Launch()
	  {
		  try 
		  {
		   d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		   d.get(url);
		   d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		   d.manage().window().maximize();
		  } 
		  catch (Exception e)
		  {
				System.out.println(e);
		  }
	  }
	  
	 @AfterTest
	  public void Close()
	  {
		  d.close();
	  }
	
	}


